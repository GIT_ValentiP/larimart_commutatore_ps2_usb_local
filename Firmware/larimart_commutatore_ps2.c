/**********************************************************************
;                                                                     *
;    Filename:	    hubps2.c,									                      *
;    Date:          26/9/05                                           *
;    File Version:  2.00                                              *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Files required:                                                  *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:                                                           *
;			Hub PS/2 per tastiere, piu tastierino 12 tasti (con tasti riprogrammabili da RS232)
;				(lavoro per K-tronic/Larimart)
;				aggiornamento da versione assembler PIC16Fx74
;                                                                     *
;**********************************************************************
;                                                                     *
;**********************************************************************
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:  2020-09-29   messo bat a 7s + portato primo bat a 1.05s        
;                                                                     *
;**********************************************************************
;                                                                     *
;*********************************************************************/


#include <p18cxxx.h>        // processor specific variable definitions
//	__CONFIG _CP_OFF & _WDT_ON & _BODEN_ON & _PWRTE_ON & _HS_OSC  & _LVP_OFF	; non c'e' MCLRE...

#include <timers.h>
#include <usart.h>
#include <portb.h>
#include <reset.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <delays.h>
#include <typedefs.h>
#include "ps2.h"
#include "io_cfg.h"


#ifdef __18F452

#pragma config WDT = ON, WDTPS = 128, LVP=OFF, STVR=ON
#pragma config OSC=HSPLL, PWRT=ON, BOR=ON, BORV=27
#pragma config CCP2MUX=ON, DEBUG=OFF

#endif

#ifdef __18F4520

#pragma config WDT = OFF, WDTPS = 32768, MCLRE=ON, STVREN=ON, LVP=OFF
#pragma config OSC=HSPLL, FCMEN=OFF, IESO=OFF, PWRT=ON, BOREN=SBORDIS, BORV=21
#pragma config LPT1OSC=OFF, PBADEN=OFF, CCP2MX=PORTC, XINST=OFF, DEBUG=OFF,XINST=ON

#endif



//	__IDLOCS 0x4744


// 100nS/istruzione @ 10MHz (PLL x4)


#define TMR0BASE  (65536-1562)	//	v. interlin_18!!! TMR0 viaggia a 64presc*(10MHz (100nS)), io voglio un IRQ ogni 10mS 


enum {
	BEEP_STD_FREQ=100,
	BEEP_LOW_FREQ=250,
	BEEP_HIGH_FREQ=50
	};




#define RITARDO_TASTIERA_POR 200
#define RITARDO_TASTIERA_BAT 400
#define RITARDO_MOUSE_POR 200
#define RITARDO_MOUSE_BAT 100


#define DIVIDER_BASE 10000


struct COMM_STATUS
{
// errori/flag in CommStatus Seriale
	unsigned int FRAME_2SEND:1;         // c'e' da mandare un frame
	unsigned int WAIT_4ACK  :1;
	unsigned int FRAME_REC  :1;         // c'e' un frame ricevuto
	unsigned int COMM_OVRERR:1;			// overrun seriale 
	unsigned int COMM_OVL   :1;         // errore di overflow buffer in ricezione 
	unsigned int COMM_TOUT  :1;         // errore di timeout in ricezione frame
	unsigned int COMM_FRERR :1;         // errore di framing (START/STOP) in ricezione 
	unsigned int COMM_PERR  :1;         // errore di parita' in ricezione 
};

enum {
	ID_BYTE0=0xAB,
	ID_BYTE1=0x83
	};
enum {
	DEVICE_ID	= 0		// 0 per standard PS/2 mouse, 3=Intellimouse, 4=Intellimouse con 4-5 pulsanti
	};
enum {
	RESET_MODE  = 0,
	RESET_MODE_PENDING,			// per delay asincrono!
	STREAM_MODE, 
	REMOTE_MODE,
	WRAP_MODE
	};


//#define USA_BUZZER 1
//#define USA_TYPEMATIC 1
#define USA_SLAVES 1
#define FORZA_ATTIVO_AL_BOOT 1

//#define DEBUG1 1			; solo ricez. slave ok
//#define DEBUG2 1			; anche ricez slave con errori
//#define DEBUG3 1			; pin C<0> toggla con il clock dello slave


// port B

#define TXClkK1Val (1 << TXClkK1Bit)
#define TXDataK1Val (1 << TXDataK1Bit)
#define TXClkK2Val (1 << TXClkK2Bit)
#define TXDataK2Val (1 << TXDataK2Bit)
#define TXClkKIVal (1 << TXClkKIBit)
#define TXDataKIVal (1 << TXDataKIBit)
#define RXClkK1Val (1 << RXClkK1Bit)
#define RXDataK1Val (1 << RXDataK1Bit)
#define RXClkK2Val (1 << RXClkK2Bit)
#define RXDataK2Val (1 << RXDataK2Bit)
#define RXClkKIVal (1 << RXClkKIBit)
#define RXDataKIVal (1 << RXDataKIBit)


// port C
#define BeepBit			2		// PWM1
#define m_BeepBit		PORTCbits.RC2


#define TX232Bit			6		// su UART opp. I2C
#define m_TX232Bit			PORTCbits.RC6
#define RX232Bit			7		// su UART opp. I2C
#define m_RX232Bit			PORTCbits.RC7

#define BeepVal		(1 << BeepBit)


// port D  (I/O)

#define TXClkM1Val (1 << TXClkM1Bit)
#define TXDataM1Val (1 << TXDataM1Bit)
#define TXClkM2Val (1 << TXClkM2Bit)
#define TXDataM2Val (1 << TXDataM2Bit)
#define TXClkMIVal (1 << TXClkMIBit)
#define TXDataMIVal (1 << TXDataMIBit)
#define RXClkM1Val (1 << RXClkM1Bit)
#define RXDataM1Val (1 << RXDataM1Bit)
#define RXClkM2Val (1 << RXClkM2Bit)
#define RXDataM2Val (1 << RXDataM2Bit)
#define RXClkMIVal (1 << RXClkMIBit)
#define RXDataMIVal (1 << RXDataMIBit)



#define LedOVal (1 << LedOBit)
#define LedOVal2 (1 << LedOBit2)
#define Puls1Val (1 << Puls1Bit)
#define Puls2Val (1 << Puls2Bit)
#define Out1Val (1 << Out1Bit)


// port E 








#define CR           0xd
#define LF           0xa

#define FLASH_TIME   7





#define SERNUM      1000
#define VERNUMH     2
#define VERNUML     2

//
//***** VARIABLE DEFINITIONS

static rom const char CopyrightString[]= {'(','C',')',' ','K','-','t','r','o','n','i','c',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','2','0','0','2','-','2','0','1','3',' ','-',' ',' ',' ',
	' ','.',' ',' ',' ','\n','\r',' ','f','/','w',' ',
	VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0', ' ','2','4','/','0','7','/','1','3', 0 };

static rom const char CopyrDate[]={ ' ','-',' ','S','/','N',' ','0','0','0','1', CR,LF,0 };

static rom const char rom *Copyr1="(C) K-tronic       2002-2006 - \xd\xa\x0";


//Tavola dati per Rate typematic (step da 32msec)
static rom byte TypemRate[] = {
	1,1,1,1,2,2,2,2,	    // 0-7
	2,2,3,3,3,3,4,		    // 8-15
	4,4,5,5,6,7,7,7,			// 16-23
	8,9,10,11,12,13,14,15	   // 24-31
	};

#pragma udata

volatile struct COMM_STATUS CommStatus;  	// bit di stato del ricevitore

//

volatile byte Timer1;
volatile byte ByteRec;		



#pragma udata access ACCESSBANK




#pragma udata

byte TimerBuzz;
byte oldPortA;

byte RESN[2],STATE,SRATE[2],SCALING;
byte READ1,READ2,READ3,READ4;
byte Mode[2];
byte first;				// b0=primo giro tastiera, b1=primo giro mouse

byte CurrScanCodeSet;
byte TypematicRate;
byte LedStatus[2];

byte LENGTH;
byte DATA[4];

byte FLAGSK,FLAGSM[2];
byte ID;				//Current device ID

#ifdef USA_TYPEMATIC
byte kbTimerTypem;
#endif

byte SlavePresent;			// b0=1 se c'e' attaccata una tastiera slave; b1=1 se mouse slave
byte PCconnesso;
byte PCsegati;					// b0-1 =1 se ignora (tipo fili bassi all'accensione) pc/tast1&2, b2-3 per mouse1&2
byte inE0F0;						// per bloccare commutazione, quando si � a met� di un pacchetto tastiera.

volatile byte DelayK1,DelayK2,DelayM1,DelayM2;
volatile byte FlagsDelay;
byte Flagsbat7s;        // 29-09-2020    per bat a 7 secondi
word counter7s;        // 29-09-2020    per bat a 7 secondi
byte bat7sok;          // 29-09-2020    per bat a 7 secondi



byte temp;			// temporanea

#define RESET_FORCED_AT7SEC  0x12345678
byte execute_reset;
//persistent 
dword ram_key;
//bank 0 & mirrored in EEPROM!

//------------------------------------------
//       BIT ASSIGNMENT in FLAGA
//------------------------------------------
//
//
// Flag bits (FLAGS)
#define GOT_IT  FLAGS,0     // byte received from PS2 keyboard
#define IGNORE_HOST 1      // se =1, ignora lo stato di clock/data durante invio (usato al boot)
#define RCV_ERROR FLAGS,2   // error during receive routine
#define BYTE_ERR  FLAGS,3   // byte not equal to one received
#define TRANS_ERR FLAGS,4   // error while sending byte to PS2 keybrd
// FLAGS<5> e' enable/disable come nel MOUSE PS2!!

//Statemachine states
#define GETKEYBDBYTE 1     // get keyboard data
#define HNDL_E0     2      // when E0 is received, handle it
#define HNDL_F0     3      // when F0 is received, handle it
#define HNDL_EF0    4      // when F0 and E0 are received, handle it
#define HNDL_E1     5      // when E1 is received, handle it
#define STORE       6      // move BYTE into USB BUFFER
#define CLEAR       7      // clear BYTE from USB BUFFER
#define CHECKLED    8      // check LED states
#define SETLED      9      // send set LED command




void _isr(void);


byte PacketOutK(void);
#define PacketOut1ByteK(n) { DATA[0]=n;	LENGTH=0x01;	PacketOutK(); }
#define ACKOutK()  PacketOut1ByteK(0xFA)

byte PacketOutM(void);
#define PacketOut1ByteM(n) { DATA[0]=n;	LENGTH=0x01;	PacketOutM(); }
#define ACKOutM()  PacketOut1ByteM(0xFA)


byte KbHandler(byte,byte);
byte slaveKbHandler(byte);
void Kb_F6(void);
void Kb_FF(byte,byte);
void Kb_FF_2(byte);
void Kb_FF_2_sub(byte);
void Kb_FF_fine(byte );
void slave_KB_ED(void);

byte MouseHandler(byte,byte);
byte slaveMouseHandler(byte);
void Mouse_FF(byte,byte);
void Mouse_FF_2(byte);
void Mouse_FF_fine(byte );
void subSetDefault(void);
void subFF_M(byte);


#define Delay_1uS() Delay_uS(1)

// void PutByte232(byte);
// short int GetByte232(void);


void Delay_mS(unsigned int);
void Delay_uS(byte);
void Delay_S_(byte);
#define Delay_S() Delay_S_(10)				// circa 1s 

#define EEscrivi0(p) EEscrivi(p,0)               // scrive 0...

#define EEscrivi(a,b) { *((byte *)a)=b; EEscrivi_(a,b); }  // scrive W in RAM e EPROM...

void EEscrivi_(SHORTPTR,byte);
byte EEleggi(SHORTPTR);
#define EEcopiaARAM(p) { *p=EEleggi(p); }
#define EEcopiaAEEPROM(p) EEscrivi_(p,*p)

void StdBeep(void);
void ErrorBeep(void);


/****************************************************************************
*
*  high_interrupt
*  Descrizione: gestore dell'interrupt
*  Riferimento:
*  Parametri in ingresso: nessuno
*  Parametri in uscita: nessuno
*  Note:
*
****************************************************************************/
#pragma code high_vector=0x8		// solo HIGH, lavorando in Compatibility Mode
void high_interrupt(void) {
	_asm goto _isr _endasm
	}


#pragma code
#pragma interrupt _isr nosave=section("MATH_DATA"),section(".tmpdata")

/****************************************************************************
*
*  isr
*  Descrizione: gestore dell'interrupt (soltanto del Timer 0)
*  Riferimento:
*  Parametri in ingresso: nessuno
*  Parametri in uscita: nessuno
*  Note:
*
****************************************************************************/
void _isr(void) {
	
	if(INTCONbits.TMR0IF) {
		INTCONbits.TMR0IF = 0;
		WriteTimer0(TMR0BASE);			// re-inizializzo TMR0

//		m_Out1Bit ^= 1;		// check timing


//      29-09-2020    per bat a 7 secondi
        if(++counter7s>699){
            counter7s=0;
            if(Flagsbat7s==0){
              Flagsbat7s=1;
              
            }
            
        }
//		-------------------------------------------------------

		if(DelayK1) {
			DelayK1--;
			if(!DelayK1)
				FlagsDelay |= 1;
			}

		if(DelayK2) {
			DelayK2--;
			if(!DelayK2)
				FlagsDelay |= 2;
			}
		
		if(DelayM1) {
			DelayM1--;
			if(!DelayM1)
				FlagsDelay |= 4;
			}
	
		if(DelayM2) {
			DelayM2--;
			if(!DelayM2)
				FlagsDelay |= 8;
			}

		}
	}





// remaining code goes here

#pragma code


/****************************************************************************
*
*  main
*  Descrizione:
*  Riferimento:
*  Parametri in ingresso: nessuno
*  Parametri in uscita: nessuno
*  Note:
*
****************************************************************************/
void main(void) {
	overlay byte i,temp2,temp3;
	overlay word n,divider;
	overlay byte TEMP0;
//	short int ch;

//	btfss	STATUS,4 						;solo se reset vero (non watchdog!)
//	goto warm_reset
//	goto  cold_reset          ; go to beginning of program

//	if(isWDTTO())				//solo se reset vero (non watchdog!)
//		goto warm_reset;
//	else
//		goto cold_reset;         // go to beginning of program

   
        
start:
cold_reset:
     if (ram_key==RESET_FORCED_AT7SEC){
        ram_key=RESET_FORCED_AT7SEC ;   
        execute_reset=0;
    }else{
        ram_key=0;  
        execute_reset=1;
    }
	ClrWdt();				// DOPO!

//	RCONbits.NOT_BOR=1;
	StatusReset();


	LATA=255;		// led basso=acceso
	LATB=0;			// CLK e DATA bassi (pronti per output=0)
	LATC=0;
	LATD=0;			// CLK e DATA bassi (pronti per output=0)
	LATE=7;
	TRISA=0b11001101;			 // 1=input 
	TRISB=TXClkK1Val | TXDataK1Val | TXClkK2Val | TXDataK2Val | 0b11000000;		//1=input (tengo clk/data slave bassi fino al mio powerup completo)
	TRISC=0b11010000;					// 1=input e cmq. per RS232!, 0=output per PWM!
	TRISD=TXClkM1Val | TXDataM1Val | TXClkM2Val | TXDataM2Val | 0b11000000;		//1=input (tengo clk/data slave bassi fino al mio powerup completo)
	TRISE=7;			 // 1=input , non usato; OCCHIO a TRISE<4> = PSP!

//	movlw	00000111b	; pull-ups B, tmr0 enable, prescaler Timer with 1:256 prescaler FINIRE!!
//	movwf	OPTION_REG

	INTCON=0;		// disabilito tutti interrupt

	FLAGSK=1 << IGNORE_HOST;






	TRISB=TXClkK1Val | TXDataK1Val | TXClkK2Val | TXDataK2Val | TXClkKIVal | TXDataKIVal | 0b11000000;		//1=input (alzo slave)
	TRISD=TXClkM1Val | TXDataM1Val | TXClkM2Val | TXDataM2Val | TXClkMIVal | TXDataMIVal | 0b11000000;		//1=input (alzo slave)


	
#ifdef __18F4520
	ADCON0=0b00000000;
	ADCON1=0b00001111;
#else
	ADCON0=0x80;	// 10000000b;
	ADCON1=0x6;		// 00000110b;
#endif

//  29-09-2020    per bat a 7 secondi
	Flagsbat7s=0;	
	counter7s=0;		
	bat7sok=0;			
//  --------------------------------
	


	TMR0L=TMR0H=0;				//clr tmr0 & prescaler

//	movlw	00000111b	; pull-ups B, tmr0 enable, prescaler Timer with 1:256 prescaler FINIRE!!
//	movwf	OPTION_REG

	INTCON=0;			// disabilito tutti interrupt

//	OpenRB0INT(PORTB_CHANGE_INT_OFF & FALLING_EDGE_INT & PORTB_PULLUPS_ON);
	EnablePullups();

	OpenTimer0(TIMER_INT_ON & T0_16BIT & T0_SOURCE_INT & T0_PS_1_64);

	RCONbits.IPEN=0;				// interrupt in Modalit� Compatibile (16X)


/*
	OpenUSART(USART_RX_INT_ON & USART_TX_INT_OFF & USART_ASYNCH_MODE & 
		USART_EIGHT_BIT & USART_CONT_RX & USART_BRGH_HIGH,
		23);
		*/
//	baudUSART(BAUD_16_BIT_RATE & BAUD_WAKEUP_OFF & BAUD_AUTO_OFF);
	// x= (Fosc / (Brate * 16 (BRGH=1) ) ) ) -1
	// Fosc=44236800 (11.0592 XTAL)


	ClrWdt();


	
	WriteTimer0(TMR0BASE);			// inizializzo TMR0



	T1CON=0x30;		//00110000b;				// spengo Timer1

	CCP1CON=0x0c;								// TMR2: PWM mode
	CCPR1L=BEEP_STD_FREQ/2;
//	CCPR1H=BEEP_STD_FREQ/2;
	T2CON=0x03;	//00000011b;									// set prescaler T2
	// OpenPWM1(); USARE??
	PR2=BEEP_STD_FREQ;





	INTCON=0xa0;				// 10100000b		attivo IRQ


	



	ClrWdt();
	LedStatus[0]=LedStatus[1]=0;

	DelayK1=DelayK2=DelayM1=DelayM2=0;
	FlagsDelay=0;
														



    Delay_mS(180);   //  2020-09-23  aggiunto ritardo per C2D III che mette inibit fino a 1S




no_debug_init:

	Delay_mS((RITARDO_MOUSE_POR+RITARDO_TASTIERA_POR)/2);				// A POR takes a minimum of 150mS and a maximum of 2.0 sec from the time power is first applied to the keyboard...






warm_reset:
	ClrWdt();							// rientro caldo (da watchdog):


	oldPortA=PORTA;




/*
	while(1) {
		m_Out1Bit ^= 1;
		Delay_mS(2);			ok 13/12/05
		} */




	first=0;
	PCconnesso=0;
	Kb_FF(0,0);			// reset iniziale
	Kb_FF(0,1);

	FLAGSK = (1 << 5) | (1 << IGNORE_HOST);
//	Buf232Cnt=0;

	Kb_F6();						// set defaults

//	Mode=RESET_MODE;
	Mouse_FF(0,0);
	Mouse_FF(0,1);
	subSetDefault();		// anche mouse

		
	STATE=0;
	ID=DEVICE_ID;			//Default Mouse ID (= 00)

	INTCON=0xA0;							// attivo IRQ
	divider=DIVIDER_BASE;						// ogni 100mS

/*
	RCSTAbits.CREN=1;					// abilito ricevitore 232
	*/

	PCconnesso = m_Puls2Bit ? 1 : 0;			//Se il segnale di ingresso � al livello alto (Vcc) il computer di Default � il C2 altrimenti se � basso (GND) il computer di Default � C1.
//	m_LedOBit2= (PCconnesso & 1);			NO! solo debug!
	m_Out1Bit= !(PCconnesso & 1);			// invertito Larimart 30/11









//    if (execute_reset==1){//Toggle Led after restart
//       m_Out1Bit = 1;
//    }else{
//       m_Out1Bit = 0;  
//    }
	do {   

//  29-09-2020    per bat a 7 secondi
		if(Flagsbat7s==1){
		    Flagsbat7s=2;
//		m_Out1Bit = 0;
        //   m_Out1Bit ^= 1;		// check timing
		//PacketOut1ByteK(0xAA);                   
	       if (execute_reset==1){
               //execute_reset=0;
               ram_key=RESET_FORCED_AT7SEC;
              
               	goto cold_reset; //Reset();
           }   
        }	
// -----------------------------------------------------


		divider--;
		if(!divider) {



			if(!inE0F0) {
				if(!(PORTA & Puls1Val) && (oldPortA & Puls1Val)) {


				// mando ERRORE a tastiera che sto lasciando
/*				LENGTH=0x01;
				DATA[0]=0xfc;
				PacketOutK();		//...la mando...
				*/



					PCconnesso ^= 1;
		//			m_LedOBit2 = (PCconnesso & 1);		NO! solo debug!
					m_Out1Bit= !(PCconnesso & 1);		// invertito Larimart 30/11
					// si potrebbe mettere DOPO... indicherebbe + coerentemente il momento in cui il canale � cambiato


					if(SlavePresent & 2) {			// se c'� uno slave...        matteo 2010
						PS2_slaveoutM(0xF4);	// FORZO ENABLE mouse
 	Delay(10);
						PS2_slaveoutM(0xF4);	// FORZO ENABLE mouse
 	Delay(10);
						PS2_slaveoutM(0xF4);	// FORZO ENABLE mouse
 	Delay(10);
						PS2_slaveoutM(0xF4);	// FORZO ENABLE mouse
 	Delay(10);
						PS2_slaveoutM(0xF4);	// FORZO ENABLE mouse
 	Delay(10);
						i=PS2_slaveoutM(0xE8);
						if(i==0xff) 						// controllo ev. errore (piu' che altro x debug...)
							goto no_set_Mouse_slave;

						TEMP0=InputSlaveM();			// aspettare ACK?

						i=PS2_slaveoutM(RESN[PCconnesso]);
						if(i==0xff) 						// controllo ev. errore (piu' che altro x debug...)
							goto no_set_Mouse_slave;

						TEMP0=InputSlaveM();		// aspettare ACK?

						i=PS2_slaveoutM(0xF3);
						if(i == 0xff) 				// controllo ev. errore (piu' che altro x debug...)
							goto no_set_Mouse_slave;

						TEMP0=InputSlaveM();		// aspettare ACK?

						i=PS2_slaveoutM(SRATE[PCconnesso]);
						if(i == 0xff) 				// controllo ev. errore (piu' che altro x debug...)
							goto no_set_Mouse_slave;

						TEMP0=InputSlaveM();		// aspettare ACK?






no_set_Mouse_slave:
						;
						}


					if(SlavePresent & 1)			// se c'� una slave...
						slave_KB_ED();				// aggiorno led status


					}
				}
			divider=DIVIDER_BASE;
			m_LedOBit ^= 1;
			}

ModeHandler:
#ifdef USA_SLAVES
		TXClkMITris=1;							// sblocco gli slave...
		TXClkKITris=1;
#endif
Stream:
		ClrWdt();



//			m_LedOBit ^= 1;
		SlavePresent=255;				// ***** debug



		INTCONbits.GIE=1;				//Enable IRQ

		if(!m_RXDataK1Bit) {			//Check for host KB request-to-send
			if(m_RXClkK1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
				INTCONbits.GIE=0;				//Request-to-send detected
				i=PCconnesso;
				PCconnesso=0;
#ifdef USA_SLAVES
				TXClkKITris=0;				// blocco gli slave...
				TXClkMITris=0;
#endif
				TEMP0=PS2inK1();
				KbHandler(TEMP0,i);
				PCconnesso=i;
//				goto	Stream5;
				}
			}
		if(!m_RXDataK2Bit) {			//Check for host KB request-to-send
			if(m_RXClkK2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
				INTCONbits.GIE=0;				//Request-to-send detected
				i=PCconnesso;
				PCconnesso=1;
#ifdef USA_SLAVES
				TXClkKITris=0;				// blocco gli slave...
				TXClkMITris=0;
#endif
				TEMP0=PS2inK2();
				KbHandler(TEMP0,i);
				PCconnesso=i;
//				goto	Stream5;
				}
			}

Stream1:

#ifdef USA_SLAVES
		if(!m_RXDataKIBit)	{		//Check for slave request-to-send
			INTCONbits.GIE=0;					//Request-to-send detected
			TEMP0=PS2_slaveinK();
			if(!(PS2Errors & 2))
				// o fare RESEND_PS2_slaveout
				slaveKbHandler(TEMP0);
//			goto	Stream5;
			}
#endif


//		if(FLAGSK & (1 << 5)) {			//FLAGS.5 = Disable(0)/Enable(1) device
//			}

//		if(FLAGSM & (1 << 5)) {			//FLAGS.5 = Disable(0)/Enable(1) device
//			}



//					goto	ModeHandler;




		switch(Mode[0]) {
			case RESET_MODE:
				STATE=0;
				ID=DEVICE_ID;			//Default Mouse ID (= 00)
				subSetDefault();		// dovrebbe basarsi su PC connesso??

#ifndef FORZA_ATTIVO_AL_BOOT				// v. anche sotto in subSetDefault!
				if(!m_Puls1Bit)
					goto  noSetFlags;
#endif

				FLAGSM[0] |= (1 << 5);				//attivo per debug!! **************

#ifndef FORZA_ATTIVO_AL_BOOT
noSetFlags:
#endif

//				Delay_mS(50);			// ? ms Delay (emulate Self-test)


#ifdef USA_SLAVES
//				PS2_slaveoutM(0xFF);	// resetto anche lo slave (e cosi' vedo se c'e'!)
//				Delay_mS(10);		// aspetto			(non tanto che poi il PC ti "scarica"...)
//				PS2_slaveinM();			// leggo ACK
#endif

				

				Mode[0]=RESET_MODE_PENDING;
				break;

			case RESET_MODE_PENDING:
				break;

			case STREAM_MODE:
				if(!m_RXDataM1Bit) {			//Check for host Mouse request-to-send
					if(m_RXClkM1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
						INTCONbits.GIE=0;				//Request-to-send detected
						PCsegati &= ~4;
						i=PCconnesso;
						PCconnesso=0;
#ifdef USA_SLAVES
						TXClkKITris=0;				// blocco gli slave...
						TXClkMITris=0;
#endif
						TEMP0=PS2inM1();
						MouseHandler(TEMP0,i);
						PCconnesso=i;
//						goto	Stream5;
						}
					else
						PCsegati |= 4;
					}

#ifdef USA_SLAVES
				if(PCconnesso == 0) {
					if(!m_RXDataMIBit)	{		//Check for slave request-to-send
						INTCONbits.GIE=0;					//Request-to-send detected
						TEMP0=PS2_slaveinM();
						if(!(PS2Errors & 8))
							// o fare RESEND_PS2_slaveout
							slaveMouseHandler(TEMP0);
//						goto	Stream5;
						}
					}
#endif


				break;

			case REMOTE_MODE:
				if(!m_RXDataM1Bit) {			//Check for host Mouse request-to-send
					if(m_RXClkM1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
						INTCONbits.GIE=0;				//Request-to-send detected
						PCsegati &= ~4;
						i=PCconnesso;
						PCconnesso=0;
#ifdef USA_SLAVES
						TXClkKITris=0;				// blocco gli slave...
						TXClkMITris=0;
#endif
						TEMP0=PS2inM1();
						MouseHandler(TEMP0,i);
						PCconnesso=i;
//						goto	Stream5;
						}
					else
						PCsegati |= 4;
					}
				break;

			case WRAP_MODE:
Mode_wrap0:
				TEMP0=0;
				if(!m_RXDataM1Bit) {			//Check for host Mouse request-to-send
					if(m_RXClkM1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
						INTCONbits.GIE=0;				//Request-to-send detected
						PCsegati &= ~4;
						i=PCconnesso;
						PCconnesso=0;
#ifdef USA_SLAVES
						TXClkKITris=0;				// blocco gli slave...
						TXClkMITris=0;
#endif
						TEMP0=PS2inM1();
						}
					else
						PCsegati |= 4;
					}
				switch(TEMP0) {
					case 0xFF:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
						ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
						if(i == PCconnesso) {
							Mouse_FF(1,PCconnesso);
							}
						PCconnesso=i;
//						goto Stream5;
						break;
					case 0xEC:
						PacketOut1ByteM(0xEC);
						PCconnesso=i;
//						goto Stream5;			//Loop
						break;
					case 0:
						break;
					default:
						PCconnesso=i;
						break;
					}
//				goto	Mode_wrap0;
				break;
			}


		switch(Mode[1]) {
			case RESET_MODE:
				STATE=0;
				ID=DEVICE_ID;			//Default Mouse ID (= 00)
				subSetDefault();		// dovrebbe basarsi su PC connesso??

#ifndef FORZA_ATTIVO_AL_BOOT				// v. anche sotto in subSetDefault!
				if(!m_Puls1Bit)
					goto  noSetFlags;
#endif

				FLAGSM[1] |= (1 << 5);				//attivo per debug!! **************

#ifndef FORZA_ATTIVO_AL_BOOT
noSetFlags:
#endif

//				Delay_mS(50);			// ? ms Delay (emulate Self-test)


#ifdef USA_SLAVES
//				PS2_slaveoutM(0xFF);	// resetto anche lo slave (e cosi' vedo se c'e'!)
//				Delay_mS(10);		// aspetto			(non tanto che poi il PC ti "scarica"...)
//				PS2_slaveinM();			// leggo ACK
#endif

				

				Mode[1]=RESET_MODE_PENDING;
				break;

			case RESET_MODE_PENDING:
				break;

			case STREAM_MODE:
				if(!m_RXDataM2Bit) {			//Check for host Mouse request-to-send
					if(m_RXClkM2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
						INTCONbits.GIE=0;				//Request-to-send detected
						PCsegati &= ~8;
						i=PCconnesso;
						PCconnesso=1;
#ifdef USA_SLAVES
						TXClkKITris=0;				// blocco gli slave...
						TXClkMITris=0;
#endif
						TEMP0=PS2inM2();
						MouseHandler(TEMP0,i);
						PCconnesso=i;
//						goto	Stream5;
						}
					else
						PCsegati |= 8;
					}


#ifdef USA_SLAVES
				if(PCconnesso != 0) {
					if(!m_RXDataMIBit)	{		//Check for slave request-to-send
						INTCONbits.GIE=0;					//Request-to-send detected
						TEMP0=PS2_slaveinM();
						if(!(PS2Errors & 8))
							// o fare RESEND_PS2_slaveout
							slaveMouseHandler(TEMP0);
//						goto	Stream5;
						}
					}
#endif


				break;

			case REMOTE_MODE:
				if(!m_RXDataM2Bit) {			//Check for host Mouse request-to-send
					if(m_RXClkM2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
						INTCONbits.GIE=0;				//Request-to-send detected
						PCsegati &= ~8;
						i=PCconnesso;
						PCconnesso=1;
#ifdef USA_SLAVES
						TXClkKITris=0;				// blocco gli slave...
						TXClkMITris=0;
#endif
						TEMP0=PS2inM2();
						MouseHandler(TEMP0,i);
						PCconnesso=i;
//						goto	Stream5;
						}
					else
						PCsegati |= 8;
					}
				break;

			case WRAP_MODE:
Mode_wrap1:
				TEMP0=0;
				if(!m_RXDataM2Bit) {			//Check for host Mouse request-to-send
					if(m_RXClkM2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
						INTCONbits.GIE=0;				//Request-to-send detected
						PCsegati &= ~8;
						i=PCconnesso;
						PCconnesso=1;
#ifdef USA_SLAVES
						TXClkKITris=0;				// blocco gli slave...
						TXClkMITris=0;
#endif
						TEMP0=PS2inM2();
						}
					else
						PCsegati |= 8;
					}
				switch(TEMP0) {
					case 0xFF:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
						ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
						if(i == PCconnesso) {
							Mouse_FF(1,PCconnesso);
							}
						PCconnesso=i;
//						goto Stream5;
						break;
					case 0xEC:
						PacketOut1ByteM(0xEC);
						PCconnesso=i;
//						goto Stream5;			//Loop
						break;
					case 0:
						break;
					default:
						PCconnesso=i;
						break;
					}
//				goto	Mode_wrap1;
				break;
			}


fineloop:

		INTCONbits.GIE=0;		//Disable button/sensor sampling


//		TXClkTris2=0;				// blocco lo slave...


Stream5:


		if(FlagsDelay & 1) {
			i=PCconnesso;
			Kb_FF_fine(0);
			Kb_FF_2_sub(0);
			FlagsDelay &= ~1;
			PCconnesso=i;
			}
		if(FlagsDelay & 2) {
			i=PCconnesso;
			Kb_FF_fine(1);
			Kb_FF_2_sub(1);
			FlagsDelay &= ~2;
			PCconnesso=i;
			}
		if(FlagsDelay & 4) {
			i=PCconnesso;
			Mouse_FF_fine(0);
			subFF_M(0);
			FlagsDelay &= ~4;
			PCconnesso=i;
			}
		if(FlagsDelay & 8) {
			i=PCconnesso;
			Mouse_FF_fine(1);
			subFF_M(1);
			FlagsDelay &= ~8;
			PCconnesso=i;
			}


		if(divider==DIVIDER_BASE) {
			oldPortA=PORTA;
			}

		} while(1);


//	CloseUSART();

	CloseTimer0();

	}		// main


byte KbHandler(byte cmd,byte oldhost) {
	static /*overlay*/ byte i;
	static byte TEMP0;

	switch(cmd) {

		case 0xED:
//0xED - SetLed
Kb_ED:
			ACK_PS2outK();			//Unbuffered output 0xFA (acknowledge)
			LedStatus[PCconnesso]=InputK();			//Read command: Bit 0 controls the Scroll Lock, Bit 1 the Num Lock and Bit 2 the Caps lock. Bits 3 to 7 are ignored
			ACK_PS2outK();			//Unbuffered output 0xFA (acknowledge)


//		call PutByte232	; ***** debug
// PROVA/DEBUG!! ******

			if(oldhost == PCconnesso) {

				if(!(LedStatus[PCconnesso] & 2))				// led verso Vcc!
					m_LedOBit2=1;
				else
					m_LedOBit2=0;
// fare davvero?? s�, matteo 12/10/05


				if(SlavePresent & 1)				// se c'� una tastiera slave...
					slave_KB_ED();						// aggiorno led status
				}

			return 0;
			break;

		case 0xEE:
//0xEE - Echo
Kb_EE:
			PacketOut1ByteK(0xEE);
			return 0;
			break;

		case 0xF0:
//0xF0 - Set Scan Code Set
Kb_F0:
			ACK_PS2outK();			//Unbuffered output 0xFA (acknowledge)
			TEMP0=InputK();					//Read command
			if(TEMP0) {
				CurrScanCodeSet=TEMP0;

		// inoltrare a slave??
				if(oldhost == PCconnesso) {
					}

				return 0;
				}

			PacketOut1ByteK(CurrScanCodeSet);
			return 0;


#ifdef 
			call	ACKOut			;Buffered output 0xFA (acknowledge)
			Delay_us(5);
			call	PS2in				;Read byte: 01-03 which determines the Scan Code Used. Sending 00 as the second byte will return the Scan Code Set currently in Use 
			Delay_us(5);
			movfw TEMP0
			bz  Kb_F0_0
			movwf CurrScanCodeSet
			retlw 0
Kb_F0_0:
			PacketOut1ByteK(CurrScanCodeSet);
			return;
#endif

			break;

		case 0xF2:
//0xF2 - ReadID
Kb_F2:
			ACKOutK();				//Buffered output 0xFA (acknowledge)
			FLAGSK |= (1 << 5);			// Enable... (v. Microchip PS2 to USB)
			LENGTH=0x02;
			DATA[0]=ID_BYTE0;
			DATA[1]=ID_BYTE1;
			PacketOutK();		//...la mando...
			break;

		case 0xF3:
//0xF3 - Set Typematic
Kb_F3:
			ACK_PS2outK();					//Unbuffered output 0xFA (acknowledge)
			TypematicRate=InputK();		//Read command: typematic
													// b7 dev'essere=0
													// b6-b5 stabiliscono il ritardo iniz. in passi da 250mS
													// b4-b0 impostano il rate di ripetizione in step da 32mS
			ACK_PS2outK();					//Unbuffered output 0xFA (acknowledge)

			if(oldhost == PCconnesso) {

			if(SlavePresent & 1) {			// se c'� una tastiera slave...
				i=PS2_slaveoutK(0xF3);
				if(i == 0xff)					// controllo ev. errore (piu' che altro x debug...)
					goto  Kb_F3_2;

				TEMP0=InputSlaveK();				// aspettare ACK?
//		call PS2_slavein

				i=PS2_slaveoutK(TypematicRate);
				if(i == 0xff)					// controllo ev. errore (piu' che altro x debug...)
					goto  Kb_F3_2;

				TEMP0=InputSlaveK();				// aspettare ACK?
				}

Kb_F3_2:	;
				}

#ifdef USA_TYPEMATIC				; non � TESTATO! Se si usa, finire!
			rrf  TypematicRate,w
			movwf temp
			rrf temp,w
			andlw 00011000b					; deve diventare un valore tra 8 e 32 (15 � 1/2sec, v. sopra)
			addlw 8
			movwf TypematicDelay
			
			movfw TypematicRate			; non lo posso toccare fino a che non ho inoltrato a slave!
			andlw 00011111b				
			TypematicRate=GetTypemRate();
#endif

			return 0;


#ifdef 
			call	ACKOut			;Buffered output 0xFA (acknowledge)
			movlw 5
			call Delay_us
			call	PS2in				;Read byte: byte, which determines the Typematic Repeat Rate
			movfw TEMP0
			movwf TypematicRate
			movlw 5
			call Delay_us
			call	ACKOut			;Buffered output 0xFA (acknowledge)
			retlw	0x00
#endif

			break;

		case 0xF4:
//0xF4 - Enable
Kb_F4:
//			kbCode=0;					// pulisco tutto!
			ACKOutK();					//Buffered output 0xFA (acknowledge)

			if(oldhost == PCconnesso) {
				FLAGSK |= (1 << 5);			// pulisco tutto!
//			PS2_slaveoutK(0xF4);
				}

			return 0x00;
			break;

		case 0xF5:
//0xF5 - Disable
Kb_F5:
			ACKOutK();					//Buffered output 0xFA (acknowledge)

			if(oldhost == PCconnesso) {
				FLAGSK &= ~(1 << 5);
//			PS2_slaveoutK(0xF5);
				}
			return 0x00;
			break;

		case 0xF6:
//0xF6 - SetDefault
			ACKOutK();					//Buffered output 0xFA (acknowledge)

			if(oldhost == PCconnesso) {
				Kb_F6();
				}
			break;

		case 0xF7:
//0xF7 - Set All Keys Typematic ecc.
Kb_F7:
Kb_F8:
Kb_F9:
Kb_FA:

	// che fare??
			ACK_PS2outK();				//Unbuffered output 0xFA (acknowledge)
			return 0x00;

			break;

		case 0xF8:
			goto	Kb_F8;			//0xF8 - Set All Keys Make/Break
			break;

		case 0xF9:
			goto	Kb_F9;			//0xF9 - Set All Keys Make
			break;

		case 0xFA:
			goto	Kb_FA;			//0xFA - Set All Keys Typematic/Make/Break
			break;

		case 0xFB:
//0xFB - Set Key Type Typematic ecc.
Kb_FB:
Kb_FC:
Kb_FD:
			return 0x00;
			break;

		case 0xFC:
			goto	Kb_FC;			//0xFC - Set Key Type Make/Break
			break;

		case 0xFD:
			goto	Kb_FD;			//0xFD - Set Key Type Make
			break;

		case 0xFE:
//0xFE - Resend
Kb_FE:
			ACK_PS2outK();				//Unbuffered output 0xFA (acknowledge)
			PacketOutK();
//		retlw	0x00
			break;

		case 0xFF:
//0xFF - Reset
Kb_FF:
			ACKOutK();					//Buffered output 0xFA (acknowledge)

//			if(oldhost == PCconnesso) {
				Kb_FF(1,PCconnesso);
//				}


			return 0;
			break;

		default:

//Invalid Command
PS2InError:
KbError:

//Comandi sconosciuti		BOAGNO faceva cos�...
//ALTCOM	MOVLW	ACKIBM
//	CALL	TXIBM

			PacketOut1ByteK(0xFC);
			return	0xFF;
			break;

		}			// switch
	}			// KBhandler


void Kb_F6(void) {
	overlay byte i;
	static /*overlay*/ byte TEMP0;

#ifdef USA_TYPEMATIC				// non � TESTATO! Se si usa, finire!
						// circa 1/2 sec per il primo autorepeat
	TypematicDelay=15;
	TypematicRate=32-4;					; circa 4/sec i successivi   (suppongo 0..31, v. tabella)
	// DEFAULTS, da ps2.pdf: Typematic delay 500 ms.
	//												Typematic rate 10.9 cps.
#endif

	if(SlavePresent & 1) {			// se c'� una tastiera slave...
		i=PS2_slaveoutK(0xF6);
		if(i == 0xff)							// controllo ev. errore (piu' che altro x debug...)
			goto Kb_F6_2;

		TEMP0=InputSlaveK();				// aspettare ACK?
//		call PS2_slavein

		}

Kb_F6_2:
	return;
	}


void Kb_FF(byte tipo,byte nPC) {

	// dare un lampo ai led, come in quelle vere...
#ifdef USA_SLAVES
	TXClkMITris=0;							// blocco gli slave...
	TXClkKITris=0;
#endif

		// 300..500 ms Delay (emulate Self-test)
//	Delay_mS(300);				// The BAT takes a minimum of 300mSec and a maximum of 500mSec. This is in addition to the time required by the POR
	if(!tipo) {
		if(nPC)
			DelayK2=RITARDO_TASTIERA_POR/10;
		else
			DelayK1=RITARDO_TASTIERA_POR/10;
		}
	else {
		if(nPC)
			DelayK2=RITARDO_TASTIERA_BAT/10;
		else
			DelayK1=RITARDO_TASTIERA_BAT/10;
		}


//	Kb_FF_fine(nPC);
	}

void Kb_FF_fine(byte nPC) {

#ifdef USA_SLAVES
	TXClkMITris=1;							// sblocco gli slave...
	TXClkKITris=1;
#endif

	Kb_FF_2(nPC);			// reset iniziale
	}

void Kb_FF_2(byte nPC) {


#ifdef USA_SLAVES
	
	if(nPC == PCconnesso) {
		SlavePresent &= ~1;

//		btfss SlavePresent,0			; se c'� una tastiera slave...
//		retlw 0		FARLO in tutti i casi!

		// ritardino? anche xche' qua arrivo con slave_inibito (forse dovrei sbloccarlo prima di scrivergli?)
		Delay_mS(10);
		// resetto anche lo slave (e cosi' vedo se c'e'!)
		PS2_slaveoutK(0xFF);
//		movlw	4						; aspetto poco...
//		call	Delay_ms	
//		InputSlave TEMP0				; aspettare ACK? sembra di no...
//		call	PS2_slavein		; leggo ACK NO!
		Delay_mS(20);
		PS2_slaveinK();
		}

#endif

	}

void Kb_FF_2_sub(byte nPC) {

	PCconnesso=nPC;

// ci sarebbe un piccolo problema (o, in realta', una feature!):
// se CLOCK e/o DATA sono bloccati bassi all'accensione, i led resteranno accesi xche' il programma si ferma QUI in mezzo
// per risolverlo, faccio cosi'...

	// modifiche ott. 2005 x hub
	if(nPC == 0) {
		if(m_RXClkK1Bit && m_RXDataK1Bit) 
			goto Kb_FF_2__;
		}
	if(nPC != 0) {
		if(m_RXClkK2Bit && m_RXDataK2Bit) 
			goto Kb_FF_2__;
		}

	if(!(FLAGSK & (1 << IGNORE_HOST))) {

Kb_FF_2__:
	// Output Self-test passed (dopo check slave!), then KB ID... QUA SEMBRA di NO!!
		LENGTH=0x01;
		DATA[0]=0xAA;
//		movf	ID, w
//		movwf	DATA2
		PacketOutK();
		}
	}

byte slaveKbHandler(byte cmd) {
	overlay byte TEMP0;

	switch(cmd) {
		case 0xAA:

//0xAA - Power on self test passed - BAT complete
slaveKb_AA:

#ifdef USA_SLAVES
			SlavePresent |= 1;


			LATCbits.LATC0 = 1;		// ***** debug


//mandare lo stato dei led??
			slave_KB_ED();

#endif

			return 0x00;
			break;

		case 0xFA:
//0xFA - Acknowledge
slaveKb_FA:
			return 0x00;
			break;

		case 0xEE:
//0xEE - Set Echo
slaveKb_EE:
			return 0x00;
			break;

		case 0xFE:
//0xFE - Resend
slaveKb_FE:
//		call  ACK_PS2_slaveout			;Unbuffered output 0xFA (acknowledge)
//		goto	PS2_slaveOut					; ammesso che serva, forse bisogna rispedire l'intero ultimo blocchetto (che potrebbe in effetti essere un solo byte, sempre, per lo slave)
			return 0x00;
			break;

		case 0xFC:
//0xFC - Power on self test Failed !
slaveKb_FC:

// che fare??
#ifdef USA_SLAVES
//		movlw 0x0
//		movwf SlavePresent
#endif

			return 0x00;
			break;

		case 0x0:
		case 0xff:
//0x00 o 0xFF - Error or buffer overflow
slaveKb_00_FF:
			return 0x00;
			break;

		default:

			if(FLAGSK & (1 << 5))							// inoltro slave solo se sono attivo master  ***** NO ott 2005
				PacketOut1ByteK(cmd);

			if(cmd==0xe0 || cmd==0xf0)
				inE0F0=1;
			else
				inE0F0=0;

#ifdef USA_SLAVES
			SlavePresent |= 1;			// se sono qua, la slave c'e'!
//			LATCbits.LATC0 = 1;		// ***** debug
#endif

			return 0;
//		goto slaveKbError		boh?? no, mai...
			break;



//Invalid Command
slaveKbError:
//		movlw	0xFC
//		call	PacketOut1Byte
			return 0xFF;

		}			// switch
	}			// slaveKBHandler


void slave_KB_ED(void) {
	overlay byte i,TEMP0;

	i=PS2_slaveoutK(0xED);
	if(i == 0xff)							// controllo ev. errore (piu' che altro x debug...)
		goto Kb_ED_2;

	TEMP0=InputSlaveK();				// aspettare ACK?
//		call PS2_slavein

		// gli inoltra lo stato dei led
	i=PS2_slaveoutK(LedStatus[PCconnesso]);
	if(i == 0xff)							// controllo ev. errore (piu' che altro x debug...)
		goto Kb_ED_2;

	TEMP0=InputSlaveK();				// aspettare ACK?

Kb_ED_2:
				;

	}

// ---------------------------------------- routine I/O PS/2
byte MouseHandler(byte cmd,byte oldhost) {
	static /*overlay*/ byte i;
	static byte TEMP0;



		Delay_mS(10);			// ritardino? dic. 2005 x Windows 2000



#ifdef Microsoft_Intellimouse 
	if(cmd == 0xf3)
		STATE=0;				//Reset state for all commands except 0xF3
#endif

	switch(cmd) {
		case 0xE6:				//0xE6 - Set Scaling 1:1
//0xE6 - Set Scaling 1:1		
Mouse_E6:
//		call	ACKOut	;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();			//Unbuffered output 0xFA (acknowledge)
			FLAGSM[PCconnesso] &= ~(1 << 4);
			return	0x00;

// AGGIUNTA x SPEDIRE a SLAVE lo SCALING (FARE?)


			break;

		case 0xE7:				//0xE7 - Set Scaling 2:1
//0xE7 - Set Scaling 2:1
Mouse_E7:
//		call	ACKOut	;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();			//Unbuffered output 0xFA (acknowledge)
			FLAGSM[PCconnesso] |= (1 << 4);
			return	0x00;

// AGGIUNTA x SPEDIRE a SLAVE lo SCALING (FARE?) (v. anche sopra - Commutazione)

			break;

		case 0xE8:				//0xE8 - Set Resolution
//0xE8 - Set Resolution
Mouse_E8:
			ACK_PS2outM();			//Unbuffered output 0xFA (acknowledge)
			RESN[PCconnesso]=InputM();
//		goto	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();			//Unbuffered output 0xFA (acknowledge)
//		retlw	0x00


			if(oldhost == PCconnesso) {

				if(SlavePresent & 2) {		// se c'� un mouse slave...
					i=PS2_slaveoutM(0xE8);
					if(i==0xff) 						// controllo ev. errore (piu' che altro x debug...)
						goto Mouse_E8_2;


					TEMP0=InputSlaveM();			// aspettare ACK?
	//		call PS2_slavein

					i=PS2_slaveoutM(RESN[PCconnesso]);
					if(i==0xff) 						// controllo ev. errore (piu' che altro x debug...)
						goto Mouse_E8_2;

					TEMP0=InputSlaveM();		// aspettare ACK?

Mouse_E8_2:
					;
					}

				}

			return 0x00;

			break;

		case 0xE9:				//0xE9 - Status Request
//0xE9 - Status Request
Mouse_E9:
			ACK_PS2outM();			//Unbuffered output 0xFA (acknowledge)
			DATA[0]=FLAGSM[PCconnesso];
			DATA[1]=RESN[PCconnesso];
			DATA[2]=SRATE[PCconnesso];
			LENGTH=0x03;
			PacketOutM();
//		retlw	0x00

			break;

		case 0xEA:				//0xEA - Set Stream Mode
//0xEA - Set Stream Mode
Mouse_EA:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
			FLAGSM[PCconnesso] &= ~(1 << 6);

//			if(oldhost == PCconnesso) {
				Mode[oldhost]=STREAM_MODE;
//				}
			return 0;

			break;

		case 0xEB:				//0xEB - Read Data
//0xEB - Read Data
Mouse_EB:
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
			DATA[0]=READ1;		//Load to-be-sent data
			DATA[1]=READ2;
			DATA[2]=READ3;
			DATA[3]=READ4;
			LENGTH=0x03;

#ifdef Microsoft_Intellimouse
			if(ID)		// If ID!=0, output 4-byte packet
				LENGTH=0x04;
#endif

			PacketOutM();

			READ2=READ3=0;				//Reset movement counters


#ifdef Microsoft_Intellimouse
			READ4 &= (ID == 0x04) ? 0xF0 : 0x00;				//Test for 5-button mode
												//If 5-button mode, don't clear button states	
												//If not 5-button mode, clear READ4
#endif
			return	0x00;		//Loop
			break;

		case 0xEC:				//0xEC - Reset Wrap Mode
//0xEC - Reset Wrap Mode
Mouse_EC:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();			//Unbuffered output 0xFA (acknowledge)

//			if(oldhost == PCconnesso) {

			if(FLAGSM[PCconnesso] & (1 << 6))
				Mode[oldhost]=REMOTE_MODE;
			else
				Mode[oldhost]=STREAM_MODE;

//				}

			return 0;
			break;

		case 0xEE:				//0xEE - Set Wrap Mode

//0xEE - Set Wrap Mode
Mouse_EE:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)

//			if(oldhost == PCconnesso) {
				Mode[oldhost]=WRAP_MODE;
//				}

			return 0;
			break;

		case 0xF0:				//0xF0 - Set Remote Mode
//0xF0 - Set Remote Mode
Mouse_F0:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
			FLAGSM[PCconnesso] |= (1 << 6);

//			if(oldhost == PCconnesso) {
				Mode[oldhost]=REMOTE_MODE;
//				}
			return 0;
			break;

		case 0xF2:				//0xF2 - Read Device Type
//0xF2 - Read Device Type
Mouse_F2:
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
			PacketOut1ByteM(ID);

			return	0x00;
			break;

		case 0xF3:				//0xF3 - Set Sampling Rate
//0xF3 - Set Sampling Rate
Mouse_F3:
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
			SRATE[PCconnesso]=InputM();
//		call	ACKOut			//Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)



			if(oldhost == PCconnesso) {
				if(SlavePresent & 2) {			// se c'� un mouse slave...

					i=PS2_slaveoutM(0xF3);
					if(i == 0xff) 				// controllo ev. errore (piu' che altro x debug...)
						goto Mouse_F3_2;

					TEMP0=InputSlaveM();		// aspettare ACK?
	//		call PS2_slavein

					i=PS2_slaveoutM(SRATE[PCconnesso]);
					if(i == 0xff) 				// controllo ev. errore (piu' che altro x debug...)
						goto Mouse_F3_2;

					TEMP0=InputSlaveM();		// aspettare ACK?

Mouse_F3_2:
					;
					}
				}


#ifdef Microsoft_Intellimouse

// Received Data       State

// -Reset State-		0
// 200			1
// 200, 100		2
// 200, 200		3
// 200, 100, 80		0	Z-wheel + 3 Button mode
// 200, 200, 80		0	Z-wheel + 5 Button mode


StateTable:
		switch(STATE) {
			case 0:
State0:
				STATE=0x01;
				if(SRATE[PCconnesso] != 200)
					STATE=0;
				return 0x00;

State1:
				STATE=0x02;
				if(SRATE[PCconnesso] != 100)
					STATE=0;
		movlw	100
		xorwf	SRATE[PCconnesso], w
		btfsc	STATUS, Z
			return 0x00;

		movlw	0x03
		movwf	STATE
				if(SRATE[PCconnesso] != 200)
					STATE=0;
		btfsc	STATUS, Z
			return 0x00;
		STATE=0;
			return 0x00;

State2:
				if(SRATE[PCconnesso] == 80)
					goto EnableID3;
				STATE=0;
				return 0x00;

State3:
				if(SRATE[PCconnesso] == 80)
					goto EnableID4;
				STATE=0;
			return 0x00;

EnableID3:
			movwf	ID=0x03;
			STATE=0;
			return 0x00;


EnableID4:
		ID=0x04;
		STATE=0;
#endif

			return 0x00;

			break;

		case 0xF4:				//0xF4 - Enable
//0xF4 - Enable
Mouse_F4:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)

			FLAGSM[PCconnesso] |= (1 << 5);
//			if(oldhost == PCconnesso) {			// proviamo a toglierlo, ma non � sicuro...
				PS2_slaveoutM(0xF4);
//				}
			return	0x00;

			break;

		case 0xF5:				//0xF5 - Disable
//0xF5 - Disable
Mouse_F5:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
			FLAGSM[PCconnesso] &= ~(1 << 5);
			return	0x00;

			break;
		case 0xF6:				//0xF6 - Set Default
//0xF6 - Set Default
Mouse_F6:
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)

			if(oldhost == PCconnesso) {
				subSetDefault();
				}
			return	0x00;
			break;

		case 0xFE:				//0xFE - Resend
//0xFE - Resend
Mouse_FE:
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)
			PacketOutM();
			return	0x00;
			break;

		case 0xFF:				//0xFF - Reset
//0xFF - Reset
//		call	ACKOut			;Buffered output 0xFA (acknowledge)
			ACK_PS2outM();		//Unbuffered output 0xFA (acknowledge)

//			if(oldhost == PCconnesso) {
				Mouse_FF(1,PCconnesso);
//				}
			return 0;
			break;

		default:					//
//Invalid Command
MouseError:
			PacketOut1ByteM(0xFC);
			return 0xFF;
			break;
		}

	}

void Mouse_FF(byte tipo,byte nPC) {


#ifdef USA_SLAVES
	TXClkKITris=0;				// blocco lo slave...
	TXClkMITris=0;
#endif



	if(!tipo) {
		if(nPC)
			DelayM2=RITARDO_MOUSE_POR/10;
		else
			DelayM1=RITARDO_MOUSE_POR/10;
		}
	else {
		if(nPC)
			DelayM2=RITARDO_MOUSE_BAT/10;
		else
			DelayM1=RITARDO_MOUSE_BAT/10;
		}



	Mode[nPC]=RESET_MODE /*RESET_MODE_PENDING*/;
	//Mouse_FF_fine(nPC);
	}

void Mouse_FF_2(byte nPC) {

#ifdef USA_SLAVES

	if(nPC == PCconnesso) {
		SlavePresent &= ~2;

		Delay_mS(10);			// ritardino?
		PS2_slaveoutM(0xFF);		// resetto anche lo slave (e cosi' vedo se c'e'!)
//		movlw	4						; aspetto poco...
//		call	Delay_ms	
//		InputSlave TEMP0				; aspettare ACK? questo blocca il PC...
//		call	PS2_slavein		; leggo ACK... NO!
		Delay_mS(20);		// aspetto			(non tanto che poi il PC ti "scarica"...)
		PS2_slaveinM();			// leggo ACK
		}

#endif

	}

void Mouse_FF_fine(byte nPC) {

#ifdef USA_SLAVES
	TXClkMITris=1;							// sblocco gli slave...
	TXClkKITris=1;
#endif

	Mouse_FF_2(nPC);
	Mode[nPC]=STREAM_MODE;
	}

void subSetDefault(void) {

	RESN[0]=RESN[1]=0x02;			//Set RESN=6 C/mm @ 320 dpi
	SRATE[0]=SRATE[1]=100;			//Set Sampling Rate = 100
	FLAGSM[0]=FLAGSM[1]=0x00;			//Set STATUS		(Default Flags: scaling 1:1, stream mode, disabled)


#ifdef FORZA_ATTIVO_AL_BOOT

	FLAGSM[0]=FLAGSM[1] |= (1 << 5);				//attivo per debug!! **************

#endif
	}

void subFF_M(byte nPC) {

	PCconnesso=nPC;

	// modifiche ott. 2005 x hub
	if(nPC == 0) {
		if(m_RXClkM1Bit && m_RXDataM1Bit)
			goto FF_2__;
		else
			PCsegati |= 4;
		}
	if(nPC != 0) {
		if(m_RXClkM2Bit && m_RXDataM2Bit)
			goto FF_2__;
		else
			PCsegati |= 8;
		}

	if(!(FLAGSK & (1 << IGNORE_HOST))) {

FF_2__:
		LENGTH=2;		//Output Self-test passed, then Mouse ID (DOPO check slave!)
		DATA[0]=0xAA;
		DATA[1]=ID;
		PacketOutM();
		}
	}

// ---------------------------------------------------------------------------
byte slaveMouseHandler(byte cmd) {
	overlay byte TEMP0;

//		movfw	TEMP0
//		call PutByte232	; ***** debug

	switch(cmd) {
		case 0:
//0x00 - Error or buffer overflow
slaveMouse_00:
			return 0x00;
			break;

		case 0xFF:
//0xFF - Error 
slaveMouse_FF:
			PS2_slaveoutM(0xFE);			//Unbuffered output RESEND
			return	0x00;
			break;

		case 0xAA:
//0xAA - Power on self test passed - BAT complete
slaveMouse_AA:

//		movlw 0xaa
//		call PutByte232	; ***** debug


			TEMP0=InputSlaveM(); 				// ID... ignorato


//		movfw TEMP0
//		call PutByte232	; ***** debug


			SlavePresent |= 2;

			LATCbits.LATC1 = 1;		// ***** debug


//	bsf PORTC,4			; ***** debug

			PS2_slaveoutM(0xF3);		//		 set sample rate
			TEMP0=InputSlaveM();		// ACK
			// il meno possibile... o rischiamo di leggere dati sporchi dallo slave!
			PS2_slaveoutM(10  /* SRATE[PCconnesso]*/);
			TEMP0=InputSlaveM();		// ACK

			PS2_slaveoutM(0xE8);		// set resolution
			TEMP0=InputSlaveM();		// ACK
			PS2_slaveoutM(RESN[PCconnesso] /*2*/);		// era 2=il meno possibile... o rischiamo di leggere dati sporchi dallo slave!
			TEMP0=InputSlaveM();		// ACK

			PS2_slaveoutM(0xE6);		// scaling
			TEMP0=InputSlaveM();		// ACK

			PS2_slaveoutM(0xF3);		// set sample rate
			TEMP0=InputSlaveM();		// ACK
			PS2_slaveoutM(SRATE[PCconnesso] /*20*/);		// era 20=il meno possibile... o rischiamo di leggere dati sporchi dallo slave!
			TEMP0=InputSlaveM();		// ACK

			PS2_slaveoutM(0xF4);		// enable
			TEMP0=InputSlaveM();		//  aspettare ACK? questo blocca il PC...
//		call PS2_slavein

			return	0x00;
			break;

		case 0xFA:
//0xFA - Acknowledge
slaveMouse_FA:
			return 0x00;
			break;

		case 0xFC:
//0xFC - Errore
slaveMouse_FC:
			return	0x00;
			break;

		case 0xFE:
//0xFE - Resend
slaveMouse_FE:
//		call  ACK_PS2_slaveout			;Unbuffered output 0xFA (acknowledge)
//		goto	PS2_slaveOut					; ammesso che serva, forse bisogna rispedire l'intero ultimo blocchetto (che potrebbe in effetti essere un solo byte, sempre, per lo slave)
			return	0x00;
			break;

		default:
			DATA[0]=cmd;

#ifdef USA_SLAVES
			SlavePresent |= 2;			// se sono qua, lo slave c'e'!
//			LATCbits.LATC0 = 1;		// ***** debug
#endif

			if((cmd & 0b00001100) != 0b00001000)			// come ulteriore sicurezza: il primo byte deve essere un byte valido
				goto error_packet_slave;

			Delay(5);
			DATA[1]=InputSlaveM();
			if(PS2Errors & 4)
				goto  error_packet_slave;
			// o fare RESEND_PS2_slaveout
			Delay(5);
			DATA[2]=InputSlaveM();
			if(PS2Errors & 4)
				goto  error_packet_slave;
			// o fare RESEND_PS2_slaveout
			Delay(5);


inputSlave4:
#ifdef Microsoft_Intellimouse		; per lo slave??
			DATA[3]=InputSlaveM();
			if(!DATA[3])
				goto inputSlave5_;
			if(DATA[3] != 0xFF)
				goto inputSlave5
inputSlave5_:
			PS2_slaveoutM(0xFE);		//Unbuffered output RESEND
			goto  inputSlave4;

inputSlave5:
#endif

#ifdef Microsoft_Intellimouse		// per lo slave??
			LENGTH=4;
#else
			LENGTH=3;
#endif




/*
			DATA[0]=0b00001000;
			DATA[1]=0b00000001;
			DATA[2]=0b00000001;*/
// prova x slave su win2000



			if(FLAGSM[PCconnesso] & (1 << 5))							// inoltro slave solo se sono attivo master			***** NO ott 2005

				PacketOutM();

#ifdef USA_SLAVES
			SlavePresent |= 2;			// se sono qua, la slave c'e'!
//			LATCbits.LATC0 = 1;		// ***** debug
#endif

			goto packet_slave_ok;

error_packet_slave:

//		movfw DATA1
//		call PutByte232	; ***** debug
//		movfw DATA2
//		call PutByte232	; ***** debug
//		movfw DATA3
//		call PutByte232	; ***** debug


packet_slave_ok:

			return 00;
//		goto slaveMouseError		boh?? no, mai...
			break;
		}



//Invalid Command
slaveMouseError:
//		movlw	0xFC
//		call	PacketOut1Byte
	return 0xFF;

	}

// ---------------------------------------- routine I/O PS/2


void ACK_PS2outK(void) {

#ifdef DEBUG_232
#ifdef USA_SLAVES	
	TXClkMITris=0;							 // blocco lo slave... (qua a volte ci arrivo in diretta (unbuffered ACK), mentre il PacketOut ci pensa x conto suo...)
	TXClkKITris=0;
#endif

	if(!PCconnesso)
		PS2outK1(0xFA);
	else
		PS2outK2(0xFA);
	
	PutByte232_HW(0xFA);

#else
#ifdef USA_SLAVES
	TXClkMITris=0;							 // blocco lo slave... (qua a volte ci arrivo in diretta (unbuffered ACK), mentre il PacketOut ci pensa x conto suo...)
	TXClkKITris=0;
#endif

	if(!PCconnesso)
		PS2outK1(0xFA);
	else
		PS2outK2(0xFA);
#endif
	}


void ACK_PS2outM(void) {

#ifdef DEBUG_232
#ifdef USA_SLAVES
	TXClkMITris=0;							 // blocco lo slave... (qua a volte ci arrivo in diretta (unbuffered ACK), mentre il PacketOut ci pensa x conto suo...)
	TXClkKITris=0;
#endif

	if(!PCconnesso)
		PS2outM1(0xFA);
	else
		PS2outM2(0xFA);
	PutByte232_HW(0xFA);

#else
#ifdef USA_SLAVES
	TXClkMITris=0;							 // blocco lo slave... (qua a volte ci arrivo in diretta (unbuffered ACK), mentre il PacketOut ci pensa x conto suo...)
	TXClkKITris=0;
#endif

	if(!PCconnesso)
		PS2outM1(0xFA);
	else
		PS2outM2(0xFA);
#endif
	}







byte PacketOutK(void) {
	static byte *p;
	static /*overlay*/ byte i,TEMP0,TEMP1;
	static byte foundInhibit;

	p=&DATA[0];			//Point to beginning of to-be-sent data buffer
	TEMP1=LENGTH;		//Number of bytes to be sent (1-4)


#ifdef USA_SLAVES
	TXClkMITris=0;							// blocco gli slave...
	TXClkKITris=0;
#endif


	do {
PacketLoop:
		ClrWdt();

		if(!PCconnesso) {
			if(!m_RXDataK1Bit) {		//Check for host request-to-send
				if(m_RXClkK1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					TEMP0=PS2inK1();					//  Read command
					KbHandler(TEMP0,0);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				}
			if(!m_RXDataM1Bit) {		//Check for host request-to-send
				if(m_RXClkM1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					PCsegati &= ~4;
					TEMP0=PS2inM1();					//  Read command
					MouseHandler(TEMP0,0);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				else
					PCsegati |= 4;
				}
			}
		else {
			if(!m_RXDataK2Bit) {		//Check for host request-to-send
				if(m_RXClkK2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					TEMP0=PS2inK2();					//  Read command
					KbHandler(TEMP0,1);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				}
			if(!m_RXDataM2Bit) {		//Check for host request-to-send
				if(m_RXClkM2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					PCsegati &= ~8;
					TEMP0=PS2inM2();					//  Read command
					MouseHandler(TEMP0,1);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				else
					PCsegati |= 8;
				}
			}
		if(foundInhibit) {
			foundInhibit--;
			if(!foundInhibit)
				goto PacketOut2;		// Abort transmission
			Delay(100);
			}


		if(!PCconnesso) {

			if(!m_RXClkK1Bit)	{			//Check for host inhibit
				if(!foundInhibit)
					foundInhibit=200;
				goto PacketLoop;			//  Check for host request-to-send
				}
											//Read byte from buffer
			i=PS2outK1(*p);			//  Output that byte
			}
		else {

			if(!m_RXClkK2Bit)	{			//Check for host inhibit
				if(!foundInhibit)
					foundInhibit=200;
				goto PacketLoop;			//  Check for host request-to-send
				}
											//Read byte from buffer
			i=PS2outK2(*p);			//  Output that byte
			}

		if(i ==	0xFF)				//If inhibited, start over (and check for RTS)
//		goto PacketOut();
			goto PacketLoop;

//		ERA PacketOUT, ma � sbagliato! Se il PC mi ha inibito, devo ripetere SOLO la PARTE RIMANENTE, non tutto!!
//		bz	PacketLoop		; SEMBRA GIUSTO QUESTO!! v. mousejoy2/hub

		if(i ==	0xFE)				//If errore (inibito male/segato), esce
			goto PacketOut2;

		p++;			//Increment pointer
		} while(--TEMP1);			//Last byte?

	return 0;						// return OK

PacketOut2:
	return 0xff;
	}

byte PacketOutM(void) {
	static byte *p;
	static /*overlay*/ byte i,TEMP0,TEMP1;
	static byte foundInhibit;


	foundInhibit=0;

	p=&DATA[0];			//Point to beginning of to-be-sent data buffer
	TEMP1=LENGTH;		//Number of bytes to be sent (1-4)


#ifdef USA_SLAVES
	TXClkMITris=0;							// blocco gli slave...
	TXClkKITris=0;
#endif


	do {
PacketLoop:
		ClrWdt();

		if(!PCconnesso) {
			if(!m_RXDataK1Bit) {		//Check for host request-to-send
				if(m_RXClkK1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					TEMP0=PS2inK1();					//  Read command
					KbHandler(TEMP0,0);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				}
			if(!m_RXDataM1Bit) {		//Check for host request-to-send
				if(m_RXClkM1Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					PCsegati &= ~4;
					TEMP0=PS2inM1();					//  Read command
					MouseHandler(TEMP0,0);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				else
					PCsegati |= 4;
				}
			}
		else {
			if(!m_RXDataK2Bit) {		//Check for host request-to-send
				if(m_RXClkK2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					TEMP0=PS2inK2();					//  Read command
					KbHandler(TEMP0,1);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				}
			if(!m_RXDataM2Bit) {		//Check for host request-to-send
				if(m_RXClkM2Bit) {			//MA IL CLOCK DEV'ESSERE ALTO (SE NO=PC SPENTO!)
					PCsegati &= ~8;
					TEMP0=PS2inM2();					//  Read command
					MouseHandler(TEMP0,1);			//  Handle command
					goto PacketOut2;		// Abort transmission
					}
				else
					PCsegati |= 8;
				}
			}
		if(foundInhibit) {
			foundInhibit--;
			if(!foundInhibit)
				goto PacketOut2;		// Abort transmission
			Delay(100);
			}

		if(!PCconnesso) {

			if(!m_RXClkM1Bit) {				//Check for host inhibit
				if(!foundInhibit)
					foundInhibit=200;
				goto PacketLoop;			//  Check for host request-to-send
				}
											//Read byte from buffer
			i=PS2outM1(*p);			//  Output that byte
			}
		else {

			if(!m_RXClkM2Bit)	{			//Check for host inhibit
				if(!foundInhibit)
					foundInhibit=200;
				goto PacketLoop;			//  Check for host request-to-send
				}
											//Read byte from buffer
			i=PS2outM2(*p);			//  Output that byte
			}

		if(i ==	0xFF)				//If inhibited, start over (and check for RTS)
//		goto PacketOut();
			goto PacketLoop;

//		ERA PacketOUT, ma � sbagliato! Se il PC mi ha inibito, devo ripetere SOLO la PARTE RIMANENTE, non tutto!!
//		bz	PacketLoop		; SEMBRA GIUSTO QUESTO!! v. mousejoy2/hub

		if(i ==	0xFE)				//If errore (inibito male/segato), esce
			goto PacketOut2;

		p++;			//Increment pointer
		} while(--TEMP1);			//Last byte?

	return 0;						// return OK

PacketOut2:
	return 0xff;
	}

byte PS2InError() {			// per ps2.c

	return 0xff;
	}


// ---------------------------------------------------------------------

void StdBeep(void) {

	CCPR2L=BEEP_STD_FREQ/2;
	CCPR2H=BEEP_STD_FREQ/2;
	PR2=BEEP_STD_FREQ;

	T2CONbits.TMR2ON=1;
	Delay_S();
	T2CONbits.TMR2ON=0;
	}

void ErrorBeep(void) {

	CCPR2L=BEEP_STD_FREQ/2+30;
	CCPR2H=BEEP_STD_FREQ/2+30;
	PR2=BEEP_STD_FREQ+60;

	T2CONbits.TMR2ON=1;
	Delay_S();
	T2CONbits.TMR2ON=0;
	}





//	messg "Numero di serie: SERNUM"




// ---------------------------------------------------------------------









// ---------------------------------------------------------------------
BOOL GetParita(byte n) {
	byte p=0;

#ifdef 
	p=n >> 1;			// Microchip AN774
	p ^= n;
	n= p >> 2;
	p ^= n;
	n= p >> 4;
	p ^= n;
	return (p & 1);
#endif

	if(n & 1)
		p++;
	if(n & 2)
		p++;
	if(n & 4)
		p++;
	if(n & 8)
		p++;
	if(n & 16)
		p++;
	if(n & 32)
		p++;
	if(n & 64)
		p++;
	if(n & 128)
		p++;
  return p & 1;
	}




// ---------------------------------------------------------------------



//Delays W microseconds (includes movlw, call, and return) @ 40MHz
void Delay_uS(byte uSec) {

	do {
		ClrWdt();			// 1
		Delay1TCY();			// TARARE!! @40MHz??
		Delay1TCY();
		Delay1TCY();
		Delay1TCY();
#ifdef __18F4550
		Delay1TCY();			// METTERE SE A 12 MHz O 10 MHz
#endif
		} while(--uSec);

  //return             ; 1
	}



//Delays approx W milliseconds
void Delay_mS(unsigned int mSec) {
	
		// usare Delay10KTCY();			// TARARE!! @40MHz??

	do {
		Delay_uS(200);
		Delay_uS(200);
		Delay_uS(200);
		Delay_uS(200);
		Delay_uS(200);
		}	while(--mSec);


//	return;
	}





void Delay_S_(byte n) {				// circa n*100mSec

	do {
	  Delay_mS(100);
		} while(--n);
	}



// -------------------------------------------------------------------------------------
void EEscrivi_(SHORTPTR addr,byte n) {

	EEADR = (byte)addr;
	EEDATA=n;

	EECON1bits.EEPGD=0;		// Point to Data Memory
	EECON1bits.CFGS=0;		// Access EEPROM
	EECON1bits.WREN=1;

//	INTCONbits.GIE = 0;			// disattiva interrupt globali
	EECON2=0x55;		 // Write 55h
	EECON2=0xAA;		 // Write AAh
	EECON1bits.WR=1;									// abilita write.
	do {
		ClrWdt();
		} while(EECON1bits.WR);							// occupato ? 

//	INTCONbits.GIE = 1;			// attiva interrupt globali

	EECON1bits.WREN=0;								// disabilita write.
  }

byte EEleggi(SHORTPTR addr) {
	byte n;

	EEADR=(byte)addr;			// Address to read
	EECON1bits.EEPGD=0;		// Point to Data Memory
	EECON1bits.CFGS=0;		// Access EEPROM
	EECON1bits.RD=1;		// EE Read
	return EEDATA;				// W = EEDATA
	}




// ---------------------------------------------------------------------------------------


short int GetByte232() {                // torna -1 (0xffff) se niente oppure il byte letto

  ClrWdt();
  if(!DataRdyUSART())
		return 0xffff;

	if(RCSTAbits.FERR)
		CommStatus.COMM_FRERR=1;
	if(RCSTAbits.OERR) {
		RCSTAbits.CREN=0;
		Nop();
		RCSTAbits.CREN=1;
//	bsf CommStatus,COMM_OVRERR				; serve l'overrun??
		}

  temp=RCSTA;								//salvare RCSTA PRIMA di leggere RCREG (v. datasheet) e tempC e' usato da GetParita
  ByteRec=RCREG 	/*ReadUSART()*/;						//ricevo solo 1 byte per volta!
// usare union USART... 

	if(GetParita(ByteRec)) {
		if(!(temp & (1 << 0 /*RX9D*/))) {
			CommStatus.COMM_PERR=1;
			}
		}
	else {
		if(temp & (1 << 0 /*RX9D*/)) {
			CommStatus.COMM_PERR=1;
			}
		}

  return ByteRec;

	}



/*short int GetByte232() {                // torna 0 e NC se niente oppure C e il byte in A
	byte ch;

  ClrWdt();
	if(Buf232Ptr == Buf232PtrI) {
		return -1;
		}

	ch=Buf232[Buf232PtrI++];
	Buf232PtrI &= 0xf;				// max 16
	return ch;
	}*/


void PutByte232(byte Byte2Send) {                // manda un byte

	do {
		ClrWdt();
		} while(!PIR1bits.TXIF);			// aspetto che il buffer di TX sia libero (v. anche MandaHdr)

	WriteUSART(Byte2Send);

	do {
		ClrWdt();
		} while(BusyUSART());

	}



