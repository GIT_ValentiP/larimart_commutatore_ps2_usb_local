// PS/2 routines comuni - 04/5/03 - 13/7/04 - 28/2/2012, 12/2012
// verione C 1/3/2005

// PARAMETRIZZARE i BIT e le PORTE usate!!


#include <p18cxxx.h>
#include "ps2.h"
#include "io_cfg.h"

typedef unsigned char byte;

/*
#define TXClkBit	1		// Si preimpostano gli out a 0, poi per portarli a uno(OpenCollector) si mettono a input...
#define m_TXClkBit	PORTBbits.RB1
#define RXClkBit	1		// (stesso filo)
#define m_RXClkBit	PORTBbits.RB1		// (stesso filo)
#define TXDataBit	0		// ...per portarli 0 si mettono a output
#define m_TXDataBit	PORTBbits.RB0
#define RXDataBit	0		// (stesso filo)
#define m_RXDataBit	PORTBbits.RB0
#define TXClkBit2	2
#define m_TXClkBit2	PORTBbits.RB2
#define RXClkBit2	2
#define m_RXClkBit2	PORTBbits.RB2
#define TXDataBit2	3
#define m_TXDataBit2	PORTBbits.RB3
#define RXDataBit2	3
#define m_RXDataBit2	PORTBbits.RB3

#define TXClkTris	TRISBbits.TRISB1		// Si preimpostano gli out a 0, poi per portarli a uno(OpenCollector) si mettono a input...
#define RXClkTris	TRISBbits.TRISB1		// (stesso filo)
#define TXDataTris	TRISBbits.TRISB0		// ...per portarli 0 si mettono a output
#define RXDataTris	TRISBbits.TRISB0		// (stesso filo)
#define TXClkTris2	TRISBbits.TRISB2
#define RXClkTris2	TRISBbits.TRISB2
#define TXDataTris2	TRISBbits.TRISB3
#define RXDataTris2	TRISBbits.TRISB3
*/

#pragma udata

byte PS2Errors;			// b0=errori master, b1=errori slave, b2-3 idem x mouse
extern byte SlavePresent;


#pragma code

//---------------------------------------------------------------------------------------------
//Wait for host to send data, then read one byte of data

byte InputK(void) {
	overlay byte masterInTimer;

	masterInTimer=255;
	if(!PCconnesso) {
		while(m_RXDataK1Bit && masterInTimer) {
			ClrWdt();
			Delay(5);
			masterInTimer--;
			}
		if(masterInTimer)
			return PS2inK1();
		else
			return 0;
		}
	else {
		while(m_RXDataK2Bit && masterInTimer) {
			ClrWdt();
			Delay(5);
			masterInTimer--;
			}
		if(masterInTimer)
			return PS2inK2();
		else
			return 0;
		}
	}

byte InputM(void) {
	overlay byte masterInTimer;

	masterInTimer=255;

	if(!PCconnesso) {
		while(m_RXDataM1Bit && masterInTimer) {
			ClrWdt();
			Delay(5);
			masterInTimer--;
			}
		if(masterInTimer)
			return PS2inM1();
		else
			return 0;
		}
	else {
		while(m_RXDataM2Bit && masterInTimer) {
			ClrWdt();
			Delay(5);
			masterInTimer--;
			}
		if(masterInTimer)
			return PS2inM2();
		else
			return 0;
		}
	}

//---------------------------------------------------------------------------------------------
//Wait for slave to send data, then read one byte of data

byte InputSlaveM(void) {
	overlay byte slaveInTimer;

	slaveInTimer=255;
	PS2Errors &= ~4;

	do {
		ClrWdt();
		if(!m_RXDataMIBit) {
			return PS2_slaveinM();
			}

		Delay(5);
		} while(--slaveInTimer);

	PS2Errors |= 4;
	return 0;
	}

byte InputSlaveK(void) {
	overlay byte slaveInTimer;

	slaveInTimer=255;
	PS2Errors &= ~1;

	do {
		ClrWdt();
		if(!m_RXDataKIBit) {
			return PS2_slaveinK();
			}

		Delay(5);
		} while(--slaveInTimer);

	PS2Errors |= 1;
	return 0;
	}



//---------------------------------------------------------------------------------------------
//When this routine is called, CLOCK must have already been pulled low by the host
byte PS2inK1(void) {
	overlay byte COUNTER,PARITY,i,TEMP0,masterInTimer;
//;		clrf  masterInTimer
//PS2in_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2in_3

//		Delay 100
//		decfsz masterInTimer,f
//		goto  PS2in_1
//		retlw 0x00

//PS2in_3:

	masterInTimer=255;
	while(!m_RXClkK1Bit && masterInTimer) {
		ClrWdt();
		Delay(1);
		masterInTimer--;
		}
	if(!masterInTimer)
		return 0x00;

	Delay(1);
	if(m_RXDataK1Bit)
		return 0x00;

	COUNTER=0x08;			//Setup a counter
	PARITY=0;
	TEMP0 = 0;	

	do {
		i=PS2inBitK1();
		if(!m_RXClkK1Bit)		//Host abort?
			return	0x00;				//  Yes--return (00=errore)
		TEMP0 >>= 1;	
//			TEMP0 &= 0x7f;	
		TEMP0 |= i;	
		PARITY ^= i;
		} while(--COUNTER);

	i=PS2inBitK1();		//Parity Bit
	if(!m_RXClkK1Bit)		//Host abort?
		return	0x00;				//  Yes--return (00=errore)
	PARITY ^= i;

	do {
		i=PS2inBitK1();		//Stop Bit
		if(!m_RXClkK1Bit)			//Host abort?
			return	0x00;					//  Yes--return (00=errore)
		} while(!m_RXDataK1Bit);		//Host released DATA?
		//  Nope--clock again

	Delay(33);			//Acknowledge
	TXDataK1Tris=0;
	Delay(5);
	TXClkK1Tris=0;
	Delay(40);
	TXClkK1Tris=1;
	Delay(5);
	TXDataK1Tris=1;
	Delay(40+200);			// NON C'ERA!! poi era 1 (e andava x il mouse...) poi portato a 20 con PC Siemens!   
 	Delay(100);             // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
							// In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************

	if(PARITY & 0x80)
		goto	noError;

	i=PS2InError();
// IN LARIMART NON C'ERA!!! PROVARE A TOGLIERLO!


	TEMP0=0;

noError:


#ifdef DEBUG_232
//	movfw	TEMP0					FARE!!! v. interlin.asm
//	call PutByte232_HW
#endif
//		call PutByte232	; ***** debug
//		movfw	TEMP0				; recupero


	return TEMP0;
	}

byte PS2inBitK1() {

	Delay(30);
	TXClkK1Tris=0;
	Delay(40);
	TXClkK1Tris=1;
	Delay(5);
//	Delay(10+5);			//2012 ... PROVARE? (da tastierino)
	return m_RXDataK1Bit ? 0x80 : 0x00;
	}

byte PS2inK2(void) {
	overlay byte COUNTER,PARITY,i,TEMP0,masterInTimer;
//;		clrf  masterInTimer
//PS2in_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2in_3

//		Delay 100
//		decfsz masterInTimer,f
//		goto  PS2in_1
//		retlw 0x00

//PS2in_3:

	masterInTimer=255;
	while(!m_RXClkK2Bit && masterInTimer) {
		ClrWdt();
		Delay(1);
		masterInTimer--;
		}
	if(!masterInTimer)
		return 0x00;


	Delay(1);
	if(m_RXDataK2Bit)
		return 0x00;

	COUNTER=0x08;			//Setup a counter
	PARITY=0;
	TEMP0 = 0;	

	do {
		i=PS2inBitK2();
		if(!m_RXClkK2Bit)		//Host abort?
			return	0x00;				//  Yes--return (00=errore)
		TEMP0 >>= 1;	
//			TEMP0 &= 0x7f;	
		TEMP0 |= i;	
		PARITY ^= i;
		} while(--COUNTER);

	i=PS2inBitK2();		//Parity Bit
	if(!m_RXClkK2Bit)		//Host abort?
		return	0x00;				//  Yes--return (00=errore)
	PARITY ^= i;

	do {
		i=PS2inBitK2();		//Stop Bit
		if(!m_RXClkK2Bit)			//Host abort?
			return	0x00;					//  Yes--return (00=errore)
		} while(!m_RXDataK2Bit);		//Host released DATA?
		//  Nope--clock again

	Delay(33);			//Acknowledge
	TXDataK2Tris=0;
	Delay(5);
	TXClkK2Tris=0;
	Delay(40);
	TXClkK2Tris=1;
	Delay(5);
	TXDataK2Tris=1;
	Delay(40+200);			// NON C'ERA!! poi era 1 (e andava x il mouse...) poi portato a 20 con PC Siemens!   
 	Delay(100);             // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
							// In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************

	if(PARITY & 128)
		goto	noError;

	i=PS2InError();
// IN LARIMART NON C'ERA!!! PROVARE A TOGLIERLO!


	TEMP0=0;

noError:


#ifdef DEBUG_232
//	movfw	TEMP0					FARE!!! v. interlin.asm
//	call PutByte232_HW
#endif
//		call PutByte232	; ***** debug
//		movfw	TEMP0				; recupero


	return TEMP0;
	}

byte PS2inBitK2(void) {

	Delay(30);
	TXClkK2Tris=0;
	Delay(40);
	TXClkK2Tris=1;
	Delay(5);
//	Delay(10+5);			//2012 NON USATO DA MATTEO... PROVARE?
	return m_RXDataK2Bit ? 0x80 : 0x00;
	}


// --------------------------------
byte PS2inM1(void) {
	overlay byte COUNTER,PARITY,i,TEMP0,masterInTimer;
//;		clrf  masterInTimer
//PS2in_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2in_3

//		Delay 100
//		decfsz masterInTimer,f
//		goto  PS2in_1
//		retlw 0x00

//PS2in_3:

	masterInTimer=255;
	while(!m_RXClkM1Bit && masterInTimer) {
		ClrWdt();
		Delay(1);
		masterInTimer--;
		}
	if(!masterInTimer)
		return 0x00;

	Delay(1);
	if(m_RXDataM1Bit)
		return 0x00;

	COUNTER=0x08;			//Setup a counter
	PARITY=0;
	TEMP0 = 0;	

	do {
		i=PS2inBitM1();
		if(!m_RXClkM1Bit)		//Host abort?
			return	0x00;				//  Yes--return (00=errore)
		TEMP0 >>= 1;	
//			TEMP0 &= 0x7f;	
		TEMP0 |= i;	
		PARITY ^= i;
		} while(--COUNTER);

	i=PS2inBitM1();		//Parity Bit
	if(!m_RXClkM1Bit)		//Host abort?
		return	0x00;				//  Yes--return (00=errore)
	PARITY ^= i;

	do {
		i=PS2inBitM1();		//Stop Bit
		if(!m_RXClkM1Bit)			//Host abort?
			return	0x00;					//  Yes--return (00=errore)
		} while(!m_RXDataM1Bit);		//Host released DATA?
		//  Nope--clock again

	Delay(33);			//Acknowledge
	TXDataM1Tris=0;
	Delay(5);
	TXClkM1Tris=0;
	Delay(40);
	TXClkM1Tris=1;
	Delay(5);
	TXDataM1Tris=1;
	Delay(40+200);			// NON C'ERA!! poi era 1 (e andava x il mouse...) poi portato a 20 con PC Siemens!   
 	Delay(100);             // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
							// In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************

	if(PARITY & 128)
		goto	noError;

	i=PS2InError();
// IN LARIMART NON C'ERA!!! PROVARE A TOGLIERLO!


	TEMP0=0;

noError:


#ifdef DEBUG_232
//	movfw	TEMP0					FARE!!! v. interlin.asm
//	call PutByte232_HW
#endif
//		call PutByte232	; ***** debug
//		movfw	TEMP0				; recupero


	return TEMP0;
	}

byte PS2inBitM1(void) {

	Delay(30);
	TXClkM1Tris=0;
	Delay(40);
	TXClkM1Tris=1;
	Delay(5);
//	Delay(10+5);			//2012 NON USATO DA MATTEO... PROVARE?
	return m_RXDataM1Bit ? 0x80 : 0x00;
	}

byte PS2inM2(void) {
	overlay byte COUNTER,PARITY,i,TEMP0,masterInTimer;
//;		clrf  masterInTimer
//PS2in_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2in_3

//		Delay 100
//		decfsz masterInTimer,f
//		goto  PS2in_1
//		retlw 0x00

//PS2in_3:

	masterInTimer=255;
	while(!m_RXClkM2Bit && masterInTimer) {
		ClrWdt();
		Delay(1);
		masterInTimer--;
		}
	if(!masterInTimer)
		return 0x00;

	Delay(1);
	if(m_RXDataM2Bit)
		return 0x00;

	COUNTER=0x08;			//Setup a counter
	PARITY=0;
	TEMP0 = 0;	

	do {
		i=PS2inBitM2();
		if(!m_RXClkM2Bit)		//Host abort?
			return	0x00;				//  Yes--return (00=errore)
		TEMP0 >>= 1;	
//			TEMP0 &= 0x7f;	
		TEMP0 |= i;	
		PARITY ^= i;
		} while(--COUNTER);

	i=PS2inBitM2();		//Parity Bit
	if(!m_RXClkM2Bit)		//Host abort?
		return	0x00;				//  Yes--return (00=errore)
	PARITY ^= i;

	do {
		i=PS2inBitM2();		//Stop Bit
		if(!m_RXClkM2Bit)			//Host abort?
			return	0x00;					//  Yes--return (00=errore)
		} while(!m_RXDataM2Bit);		//Host released DATA?
		//  Nope--clock again

	Delay(33);			//Acknowledge
	TXDataM2Tris=0;
	Delay(5);
	TXClkM2Tris=0;
	Delay(40);
	TXClkM2Tris=1;
	Delay(5);
	TXDataM2Tris=1;
	Delay(40+200);			// NON C'ERA!! poi era 1 (e andava x il mouse...) poi portato a 20 con PC Siemens!   
 	Delay(100);             // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
							// In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************

	if(PARITY & 128)
		goto	noError;

	i=PS2InError();
// IN LARIMART NON C'ERA!!! PROVARE A TOGLIERLO!


	TEMP0=0;

noError:


#ifdef DEBUG_232
//	movfw	TEMP0					FARE!!! v. interlin.asm
//	call PutByte232_HW
#endif
//		call PutByte232	; ***** debug
//		movfw	TEMP0				; recupero


	return TEMP0;
	}

byte PS2inBitM2(void) {

	Delay(30);
	TXClkM2Tris=0;
	Delay(40);
	TXClkM2Tris=1;
	Delay(5);
//	Delay(10+5);			//2012 NON USATO DA MATTEO... PROVARE?
	return m_RXDataM2Bit ? 0x80 : 0x00;
	}

		
//---------------------------------------------------------------------------------------------
byte PS2outK1(byte n) {
	overlay byte COUNTER,PARITY,i,TEMP0;
	overlay byte timeout_timer;

//		clrf  masterInTimer
//PS2out_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2out_3

//	Delay 100
//		decfsz masterInTimer,f
//		goto  PS2out_1
//		retlw 0xff

//PS2out_3:

	Delay(50);			//						luglio 2004

	timeout_timer=255;
	while(!m_RXClkK1Bit && timeout_timer) {
		ClrWdt();
		Delay(5);
		timeout_timer--;
		}
	if(!m_RXClkK1Bit)
		return 0xFE;

	Delay(2);					// era 1 **************

	if(!m_RXDataK1Bit)
		return	0xFE;

	PARITY=0x01;			// ODD parity (dis-parity!)
	COUNTER=0x08;
	TEMP0=n;
	Delay(10);					// era 18 in 2003
	PS2outBitK1(0);			// Start bit
	Delay(3);

	do {
		PARITY ^= TEMP0;
		PS2outBitK1(TEMP0);
		if(!m_RXClkK1Bit) {
			PS2outEnd();
			return 0xFF;
			}
		TEMP0 >>= 1;
		} while(--COUNTER);

	Delay(5);
	PS2outBitK1(PARITY);
	Delay(5);
	PS2outBitK1(0x01);
	Delay(35+200);        // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
	Delay(100);			  // In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************
	return	0x00;
	}

void PS2outBitK1(byte n) {

	TXDataK1Tris= n & 0x01;

	Delay(18);
	TXClkK1Tris=0;
	Delay(40);
	TXClkK1Tris=1;
	Delay(15);					//25 in 2003					; era 15 IN mOUSE lARIMART!!
	}

byte PS2outK2(byte n) {
	overlay byte COUNTER,PARITY,i,TEMP0;
	overlay byte timeout_timer;

//		clrf  masterInTimer
//PS2out_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2out_3

//	Delay 100
//		decfsz masterInTimer,f
//		goto  PS2out_1
//		retlw 0xff

//PS2out_3:

	Delay(50);			//						luglio 2004

	timeout_timer=255;
	while(!m_RXClkK2Bit && timeout_timer) {
		ClrWdt();
		Delay(5);
		timeout_timer--;
		}
	if(!m_RXClkK2Bit)
		return 0xFE;

	Delay(2);					// era 1 **************

	if(!m_RXDataK2Bit)
		return	0xFE;

	PARITY=0x01;			// ODD parity (dis-parity!)
	COUNTER=0x08;
	TEMP0=n;
	Delay(10);					// era 18 in 2003
	PS2outBitK2(0);			// Start bit
	Delay(3);

	do {
		PARITY ^= TEMP0;
		PS2outBitK2(TEMP0);
		if(!m_RXClkK2Bit) {
			PS2outEnd();
			return 0xFF;
			}
		TEMP0 >>= 1;
		} while(--COUNTER);

	Delay(5);
	PS2outBitK2(PARITY);
	Delay(5);
	PS2outBitK2(0x01);
	Delay(35+200);        // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
	Delay(100);			  // In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************
	return	0x00;
	}

void PS2outBitK2(byte n) {

	TXDataK2Tris= n & 0x01;

	Delay(18);
	TXClkK2Tris=0;
	Delay(40);
	TXClkK2Tris=1;
	Delay(15);					//25 in 2003					; era 15 IN mOUSE lARIMART!!
	}

byte PS2outM1(byte n) {
	overlay byte COUNTER,PARITY,i,TEMP0;
	overlay byte timeout_timer;

//		clrf  masterInTimer
//PS2out_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2out_3

//	Delay 100
//		decfsz masterInTimer,f
//		goto  PS2out_1
//		retlw 0xff

//PS2out_3:

	Delay(50);			//						luglio 2004

	timeout_timer=255;
	while(!m_RXClkM1Bit && timeout_timer) {
		ClrWdt();
		Delay(5);
		timeout_timer--;
		}
	if(!m_RXClkM1Bit)
		return 0xFE;

	Delay(2);					// era 1 **************

	if(!m_RXDataM1Bit)
		return 0xFE;

	PARITY=0x01;			// ODD parity (dis-parity!)
	COUNTER=0x08;
	TEMP0=n;
	Delay(10);					// era 18 in 2003
	PS2outBitM1(0);			// Start bit
	Delay(3);

	do {
		PARITY ^= TEMP0;
		PS2outBitM1(TEMP0);
		if(!m_RXClkM1Bit) {
			PS2outEnd();
			return 0xFF;
			}
		TEMP0 >>= 1;
		} while(--COUNTER);

	Delay(5);
	PS2outBitM1(PARITY);
	Delay(5);
	PS2outBitM1(0x01);
	Delay(35+200);        // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
	Delay(100);			  // In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************
	return	0x00;
	}

void PS2outBitM1(byte n) {

	TXDataM1Tris= n & 0x01;

	Delay(18);
	TXClkM1Tris=0;
	Delay(40);
	TXClkM1Tris=1;
	Delay(15);					//25 in 2003					; era 15 IN mOUSE lARIMART!!
	}

byte PS2outM2(byte n) {
	overlay byte COUNTER,PARITY,i,TEMP0;
	overlay byte timeout_timer;

//		clrf  masterInTimer
//PS2out_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2out_3

//	Delay 100
//		decfsz masterInTimer,f
//		goto  PS2out_1
//		retlw 0xff

//PS2out_3:

	Delay(50);			//						luglio 2004

	timeout_timer=255;
	while(!m_RXClkM2Bit && timeout_timer) {
		ClrWdt();
		Delay(5);
		timeout_timer--;
		}
	if(!m_RXClkM2Bit)
		return 0xFE;

	Delay(2);					// era 1 **************

	if(!m_RXDataM2Bit)
		return 0xFE;

	PARITY=0x01;			// ODD parity (dis-parity!)
	COUNTER=0x08;
	TEMP0=n;
	Delay(10);					// era 18 in 2003
	PS2outBitM2(0);			// Start bit
	Delay(3);

	do {
		PARITY ^= TEMP0;
		PS2outBitM2(TEMP0);
		if(!m_RXClkM2Bit) {
			PS2outEnd();
			return 0xFF;
			}
		TEMP0 >>= 1;
		} while(--COUNTER);

	Delay(5);
	PS2outBitM2(PARITY);
	Delay(5);
	PS2outBitM2(0x01);
	Delay(35+200);        // 2012 in larimart con la scheda nuova e i filtri super  aggiunti 200 
	Delay(100);			  // In pratica, la PS2Out si sovrapponeva a PS2in (e il PC non riceveva gli ACK dalla tastiera) ************
	return	0x00;
	}

void PS2outBitM2(byte n) {

	TXDataM2Tris= n & 0x01;

	Delay(18);
	TXClkM2Tris=0;
	Delay(40);
	TXClkM2Tris=1;
	Delay(15);					//25 in 2003					; era 15 IN mOUSE lARIMART!!
	}


byte PS2outMBOTH(byte n) {
	overlay byte COUNTER,PARITY,i,TEMP0;

//		clrf  masterInTimer
//PS2out_1:
//		clrwdt
//		btfsc	PORTB,RXClkBit
//		goto  PS2out_3

//	Delay 100
//		decfsz masterInTimer,f
//		goto  PS2out_1
//		retlw 0xff

//PS2out_3:

	Delay(50);			//						luglio 2004

//	while(!m_RXClkM1Bit)
//		ClrWdt();

	Delay(2);					// era 1 **************

//	if(!m_RXDataM1Bit)
//		return	0xFF;

	PARITY=0x01;			// ODD parity (dis-parity!)
	COUNTER=0x08;
	TEMP0=n;
	Delay(10);					// era 18 in 2003
	PS2outBitMBOTH(0);			// Start bit
	Delay(3);

	do {
		PARITY ^= TEMP0;
		PS2outBitMBOTH(TEMP0);
//		if(!m_RXClkM1Bit) {
//			PS2outEnd();
//			return 0xFF;
//			}
		TEMP0 >>= 1;
		} while(--COUNTER);

	Delay(5);
	PS2outBitMBOTH(PARITY);
	Delay(5);
	PS2outBitMBOTH(0x01);
	Delay(35);
	return	0x00;
	}

void PS2outBitMBOTH(byte n) {

	TXDataM1Tris= n & 0x01;
	TXDataM2Tris= n & 0x01;

	Delay(18);
	TXClkM1Tris=0;
	TXClkM2Tris=0;
	Delay(40);
	TXClkM1Tris=1;
	TXClkM2Tris=1;
	Delay(15);					//25 in 2003					; era 15 IN mOUSE lARIMART!!
	}


byte PS2outEnd(void) {

	TXClkK1Tris=1;
	TXDataK1Tris=1;
	TXClkK2Tris=1;
	TXDataK2Tris=1;
	TXClkM1Tris=1;
	TXDataM1Tris=1;
	TXClkM2Tris=1;
	TXDataM2Tris=1;


//		Delay	25					; prova luglio 2004

	return 0xFF;
	}


//---------------------------------------------------------------------------------------------
//When this routine is called, DATA e' basso (bit di start)

byte PS2_slaveinBitK(void) {
	overlay byte i,slaveInTimer;

	slaveInTimer=250;					// ca. 1mS max (x2=5KHz min. acc.)

	do {
		ClrWdt();
		if(!m_RXClkKIBit)				// aspetto che CLK scenda...
			goto  PS2_slaveinBit2;

		Delay(2);
		} while(--slaveInTimer);
	return 0xff;

PS2_slaveinBit2:
	Delay(2);
	if(m_RXClkKIBit)					// CLK DEVE essere ancora basso!!
		return 0xff;

#ifdef DEBUG3
	LATCbits.LATC3 ^= 1;		// ***** debug
#endif

	i= m_RXDataKIBit ? 0x80 : 0x00;

	slaveInTimer=250;						// ca. 1mS max (x2=5KHz min. acc.)

	do {
		ClrWdt();
		if(m_RXClkKIBit)				// aspetto che CLK risalga...
			goto  PS2_slaveinBit4;

		Delay(2);
		} while(--slaveInTimer);
	return 0xff;

PS2_slaveinBit4:
	return i;

	}


byte PS2_slaveinK(void) {
	overlay byte i,PARITY,TEMP0,COUNTER;

	i=PS2_slaveinBitK();
	if(i == 0xff) {									// se errore...
//		clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
		PS2Errors |= 2;
		return 0xff;
		}

		
	COUNTER=0x08;		//Setup a counter
	PARITY=0;
	TEMP0=0;		// forse inutile...
	PS2Errors &= ~2;

	do {
		i=PS2_slaveinBitK();
		if(i == 0xff)	{								// se errore...
			SlavePresent=0;
//			LATCbits.LATC0 = 0;		// ***** debug
			PS2Errors |= 2;
			return 0xff;
			}

		TEMP0 >>= 1;
		TEMP0 |= i;
		PARITY ^= i;
		} while(--COUNTER);


	i=PS2_slaveinBitK();			//Parity Bit
	if(i == 0xff) {									// se errore...
//	clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
		PS2Errors |= 2;
		return 0xff;
		}

	PARITY ^= i;

	i=PS2_slaveinBitK();			//Stop Bit
	if(i == 0xff)	{								// se errore...
//		clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
		PS2Errors |= 2;
		return 0xff;
		}

	if(i == 0x80) {														// dev'essere "1"!!
		if(PARITY & 0x80) {

#ifdef DEBUG1
			PutByte232(TEMP0)	;			//***** debug
#endif

#ifdef DEBUG_232
	//	movfw	TEMP0					FARE!!! v. interlin.asm
	//	call PutByte232_HW
#endif

			return TEMP0;
			}
		}


//		call	PS2InError
// NO!! COSA DEVO FARE??
	PS2Errors |= 2;


#ifdef DEBUG2
		PutByte232(TEMP0);		// ***** debug
#endif

#ifdef DEBUG_232
//	movfw	TEMP0					FARE!!! v. interlin.asm
//	call PutByte232_HW
#endif

	return 0;

	}

//---------------------------------------------------------------------------------------------
byte PS2_slaveinBitM(void) {
	overlay byte i,slaveInTimer;

	slaveInTimer=250;					// ca. 1mS max (x2=5KHz min. acc.)

	do {
		ClrWdt();
		if(!m_RXClkMIBit)				// aspetto che CLK scenda...
			goto  PS2_slaveinBit2;

		Delay(2);
		} while(--slaveInTimer);
	return 0xff;

PS2_slaveinBit2:
	Delay(2);
	if(m_RXClkMIBit)					// CLK DEVE essere ancora basso!!
		return 0xff;

#ifdef DEBUG3
	LATCbits.LATC3 ^= 1;		// ***** debug
#endif

	i= m_RXDataMIBit ? 0x80 : 0x00;

	slaveInTimer=250;						// ca. 1mS max (x2=5KHz min. acc.)

	do {
		ClrWdt();
		if(m_RXClkMIBit)				// aspetto che CLK risalga...
			goto  PS2_slaveinBit4;

		Delay(2);
		} while(--slaveInTimer);
	return 0xff;

PS2_slaveinBit4:
	return i;

	}

byte PS2_slaveinM(void) {
	overlay byte i,PARITY,TEMP0,COUNTER;

	i=PS2_slaveinBitM();
	if(i == 0xff) {									// se errore...
//		clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
		PS2Errors |= 8;
		return 0xff;
		}

		
	COUNTER=0x08;		//Setup a counter
	PARITY=0;
	TEMP0=0;		// forse inutile...
	PS2Errors &= ~8;

	do {
		i=PS2_slaveinBitM();
		if(i == 0xff)	{								// se errore...
			SlavePresent=0;
//			LATCbits.LATC0 = 0;		// ***** debug
			PS2Errors |= 8;
			return 0xff;
			}

		TEMP0 >>= 1;
		TEMP0 |= i;
		PARITY ^= i;
		} while(--COUNTER);


	i=PS2_slaveinBitM();			//Parity Bit
	if(i == 0xff) {									// se errore...
//	clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
		PS2Errors |= 8;
		return 0xff;
		}

	PARITY ^= i;

	i=PS2_slaveinBitM();			//Stop Bit
	if(i == 0xff)	{								// se errore...
//		clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
		PS2Errors |= 8;
		return 0xff;
		}

	if(i == 0x80) {														// dev'essere "1"!!
		if(PARITY & 0x80) {

#ifdef DEBUG1
			PutByte232(TEMP0)	;			//***** debug
#endif

#ifdef DEBUG_232
	//	movfw	TEMP0					FARE!!! v. interlin.asm
	//	call PutByte232_HW
#endif

			return TEMP0;
			}
		}


//		call	PS2InError
// NO!! COSA DEVO FARE??
	PS2Errors |= 8;


#ifdef DEBUG2
		PutByte232(TEMP0);		// ***** debug
#endif

#ifdef DEBUG_232
//	movfw	TEMP0					FARE!!! v. interlin.asm
//	call PutByte232_HW
#endif

	return 0;

	}


//---------------------------------------------------------------------------------------------
		
byte PS2_slaveoutK(byte n) {
	overlay byte i,slaveInTimer,PARITY,COUNTER,TEMP0;

	TXClkKITris=0;				// blocco eventuali trasmissioni...
	Delay(100);
	TXDataKITris=0;				// inizio la trasmissione...
	Delay(2);
	TXClkKITris=1;
//		Delay 2


// il loop di ritardo che segue e' importante perche' (come da documentazione) se si cerca di scrivere alla slave mentre era inibita, questa puo' metterci fino a 10mS per iniziare a generare il clock...
	slaveInTimer=255;

	do {
		ClrWdt();
		if(!m_RXClkKIBit)				// aspetto che CLK scenda...
			goto PS2_slaveoutStart2;

		Delay(25);
		} while(--slaveInTimer);
	TXDataKITris=1;				// inizio la trasmissione...
	TXClkKITris=1;
	return 0xff;

PS2_slaveoutStart2:

//		clrw
//		call  PS2_slaveoutBit
		
	PARITY=0x01;
	COUNTER=0x08;
	TEMP0 =n;

	do {
		PARITY ^= TEMP0;
		i=PS2_slaveoutBitK(TEMP0);
		if(i == 0xff) { 									// se errore...
			TXDataKITris=1;				// fine trasmissione...
//		clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
			return 0xff;
			}

		TEMP0 >>= 1;
		} while(--COUNTER);

	PS2_slaveoutBitK(PARITY);
	PS2_slaveoutBitK(0x01);

	TXDataKITris=1;				// fine trasmissione (forse inutile xche' lo fa il stop-bit)

// aspetto ACK dalla tastiera/mouse!!
	PS2_slaveinBitK();
// controllare???
	return	0x00;
	}

byte PS2_slaveoutBitK(byte n) {
	byte slaveInTimer;
		
	slaveInTimer=255;

	do {
		ClrWdt();
		if(!m_RXClkKIBit)				// aspetto che CLK scenda...
			goto  PS2_slaveoutBit2;

		Delay(1);
		} while(--slaveInTimer);

	return 0xff;


PS2_slaveoutBit2:

	TXDataKITris = n & 1;

	slaveInTimer=255;

	do {
		ClrWdt();
		if(m_RXClkKIBit)				// aspetto che CLK salga...
			goto  PS2_slaveoutBit4;

		Delay(1);
		} while(--slaveInTimer);

	return 0xff;

PS2_slaveoutBit4:
	return 0;

	}


byte PS2_slaveoutM(byte n) {
	overlay byte i,slaveInTimer,PARITY,COUNTER,TEMP0;

	TXClkMITris=0;				// blocco eventuali trasmissioni...
	Delay(100);
	TXDataMITris=0;				// inizio la trasmissione...
//	Delay(2);
 	Delay(100);             //Matteo 15-09-2010
	TXClkMITris=1;
//		Delay 2
 	Delay(50);

// il loop di ritardo che segue e' importante perche' (come da documentazione) se si cerca di scrivere alla slave mentre era inibita, questa puo' metterci fino a 10mS per iniziare a generare il clock...
	slaveInTimer=255;

	do {
		ClrWdt();
		if(!m_RXClkMIBit)				// aspetto che CLK scenda...
			goto PS2_slaveoutStart2;

		Delay(25);
		} while(--slaveInTimer);
	TXDataMITris=1;
	TXClkMITris=1;
	return 0xff;

PS2_slaveoutStart2:							// inizio la trasmissione...

//		clrw
//		call  PS2_slaveoutBit
		
	PARITY=0x01;
	COUNTER=0x08;
	TEMP0 =n;

	do {
		PARITY ^= TEMP0;
		i=PS2_slaveoutBitM(TEMP0);
		if(i == 0xff) { 									// se errore...
			TXDataMITris=1;				// fine trasmissione...
//		clrf SlavePresent
//			LATCbits.LATC0 = 0;		// ***** debug
			return 0xff;
			}

		TEMP0 >>= 1;
		} while(--COUNTER);

	PS2_slaveoutBitM(PARITY);
	PS2_slaveoutBitM(0x01);

	TXDataMITris=1;				// fine trasmissione (forse inutile xche' lo fa il stop-bit)

// aspetto ACK dalla tastiera/mouse!!
	PS2_slaveinBitM();
// controllare???
	return	0x00;
	}

byte PS2_slaveoutBitM(byte n) {
	byte slaveInTimer;
		
	slaveInTimer=255;

	do {
		ClrWdt();
		if(!m_RXClkMIBit)				// aspetto che CLK scenda...
			goto  PS2_slaveoutBit2;

		Delay(1);
		} while(--slaveInTimer);

	return 0xff;


PS2_slaveoutBit2:

	TXDataMITris = n & 1;

	slaveInTimer=255;

	do {
		ClrWdt();
		if(m_RXClkMIBit)				// aspetto che CLK salga...
			goto  PS2_slaveoutBit4;

		Delay(1);
		} while(--slaveInTimer);

	return 0xff;

PS2_slaveoutBit4:
	return 0;

	}




