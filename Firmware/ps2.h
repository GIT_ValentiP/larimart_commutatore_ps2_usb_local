                                                                                                                                                                        #ifndef _PS2_INCLUDED
#define _PS2_INCLUDED

// PS/2 routines comuni - 04/5/03 - 13/7/04
// verione C 1/3/2005

// PARAMETRIZZARE i BIT e le PORTE usate!!


extern unsigned char PS2Errors;
extern unsigned char PCconnesso;

unsigned char InputK(void);
unsigned char InputSlaveK(void);
unsigned char InputM(void);
unsigned char InputSlaveM(void);

unsigned char PS2inK1(void);
unsigned char PS2inBitK1(void);
unsigned char PS2inK2(void);
unsigned char PS2inBitK2(void);

unsigned char PS2inM1(void);
unsigned char PS2inBitM1(void);
unsigned char PS2inM2(void);
unsigned char PS2inBitM2(void);

unsigned char PS2InError(void);

void Delay_uS(unsigned char);

	
void ACK_PS2outK(void);
void ACK_PS2outM(void);
	
unsigned char PS2outK1(unsigned char );
unsigned char PS2outM1(unsigned char );
unsigned char PS2outK2(unsigned char );
unsigned char PS2outM2(unsigned char );
unsigned char PS2outMBOTH(unsigned char n);

void PS2outBitK1(unsigned char );
void PS2outBitM1(unsigned char );
void PS2outBitK2(unsigned char );
void PS2outBitM2(unsigned char );
void PS2outBitMBOTH(unsigned char );
unsigned char PS2outEnd(void);

unsigned char PS2_slaveinK(void);
unsigned char PS2_slaveinBitK(void);
unsigned char PS2_slaveinM(void);
unsigned char PS2_slaveinBitM(void);

#define RESEND_PS2_slaveoutK() PS2_slaveoutK(0xFE)
#define RESEND_PS2_slaveoutM() PS2_slaveoutM(0xFE)

#define ACK_PS2_slaveoutK() PS2_slaveoutK(0xFA)
#define ACK_PS2_slaveoutM() PS2_slaveoutM(0xFA)
		
unsigned char PS2_slaveoutK(unsigned char );
unsigned char PS2_slaveoutBitK(unsigned char);
unsigned char PS2_slaveoutM(unsigned char );
unsigned char PS2_slaveoutBitM(unsigned char);

#define Delay(n) Delay_uS(n)

#endif
