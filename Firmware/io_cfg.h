
// PORT A:
#define Puls1Bit		0
#define m_Puls1Bit		PORTAbits.RA0
#define Out1Bit		    1
#define m_Out1Bit		LATAbits.LATA1
#define Puls2Bit		2
#define m_Puls2Bit		PORTAbits.RA2
#define LedOBit		    4
#define m_LedOBit		LATAbits.LATA4
#define LedOBit2		5
#define m_LedOBit2		LATAbits.LATA5

// port B
#define TXDataK1Bit	0
#define m_TXDataK1Bit		LATBbits.LATB0
#define RXDataK1Bit	0
#define m_RXDataK1Bit		PORTBbits.RB0
#define TXClkK1Bit	1
#define m_TXClkK1Bit		LATBbits.LATB1
#define RXClkK1Bit	1
#define m_RXClkK1Bit		PORTBbits.RB1
#define TXDataK2Bit	2
#define m_TXDataK2Bit		LATBbits.LATB2
#define RXDataK2Bit	2
#define m_RXDataK2Bit		PORTBbits.RB2
#define TXClkK2Bit	3
#define m_TXClkK2Bit		LATBbits.LATB3
#define RXClkK2Bit	3
#define m_RXClkK2Bit		PORTBbits.RB3
#define TXDataKIBit	4
#define m_TXDataKIBit		LATBbits.LATB4
#define RXDataKIBit	4
#define m_RXDataKIBit		PORTBbits.RB4
#define TXClkKIBit	5
#define m_TXClkKIBit		LATBbits.LATB5
#define RXClkKIBit	5
#define m_RXClkKIBit		PORTBbits.RB5

// port D
#define TXDataM1Bit	0
#define m_TXDataM1Bit		LATDbits.LATD0
#define RXDataM1Bit	0
#define m_RXDataM1Bit		PORTDbits.RD0
#define TXClkM1Bit	1
#define m_TXClkM1Bit		LATDbits.LATD1
#define RXClkM1Bit	1
#define m_RXClkM1Bit		PORTDbits.RD1
#define TXDataM2Bit	2
#define m_TXDataM2Bit		LATDbits.LATD2
#define RXDataM2Bit	2
#define m_RXDataM2Bit		PORTDbits.RD2
#define TXClkM2Bit	3
#define m_TXClkM2Bit		LATDbits.LATD3
#define RXClkM2Bit	3
#define m_RXClkM2Bit		PORTDbits.RD3
#define TXDataMIBit	4
#define m_TXDataMIBit		LATDbits.LATD4
#define RXDataMIBit	4
#define m_RXDataMIBit		PORTDbits.RD4
#define TXClkMIBit	5
#define m_TXClkMIBit		LATDbits.LATD5
#define RXClkMIBit	5
#define m_RXClkMIBit		PORTDbits.RD5

#define TXDataK1Tris	TRISBbits.TRISB0		// ...per portarli 0 si mettono a output
#define RXDataK1Tris	TRISBbits.TRISB0		// (stesso filo)
#define TXClkK1Tris	TRISBbits.TRISB1		// Si preimpostano gli out a 0, poi per portarli a uno(OpenCollector) si mettono a input...
#define RXClkK1Tris	TRISBbits.TRISB1		// (stesso filo)
#define TXDataK2Tris	TRISBbits.TRISB2
#define RXDataK2Tris	TRISBbits.TRISB2
#define TXClkK2Tris	TRISBbits.TRISB3
#define RXClkK2Tris	TRISBbits.TRISB3
#define TXDataKITris	TRISBbits.TRISB4
#define RXDataKITris	TRISBbits.TRISB4
#define TXClkKITris	TRISBbits.TRISB5
#define RXClkKITris	TRISBbits.TRISB5

#define TXDataM1Tris	TRISDbits.TRISD0		// ...per portarli 0 si mettono a output
#define RXDataM1Tris	TRISDbits.TRISD0		// (stesso filo)
#define TXClkM1Tris	TRISDbits.TRISD1		// Si preimpostano gli out a 0, poi per portarli a uno(OpenCollector) si mettono a input...
#define RXClkM1Tris	TRISDbits.TRISD1		// (stesso filo)
#define TXDataM2Tris	TRISDbits.TRISD2
#define RXDataM2Tris	TRISDbits.TRISD2
#define TXClkM2Tris	TRISDbits.TRISD3
#define RXClkM2Tris	TRISDbits.TRISD3
#define TXDataMITris	TRISDbits.TRISD4
#define RXDataMITris	TRISDbits.TRISD4
#define TXClkMITris	TRISDbits.TRISD5
#define RXClkMITris	TRISDbits.TRISD5

